<?php

class Base extends CI_Model
{

    public $table;
    public $p_k = null;
    public $title = null;
    public $is_state = true;
    public $state = "state";
    public $column_order = array();
    public $column_search = array();
    public $col_where = array();
    public $where = array();
    public $order = array();
    public $joinTable = null;
    public $joinCol = null;
    public $view = array();
    public $view_col = array();
    public $options = array();

    function get_list($is_array = false)
    {
        if ($this->is_state) {
            $this->db->where($this->state, 1);
        }
        if ($this->title != null && $this->p_k != null)
            $this->db->select('' . $this->title . ' as text , ' . $this->p_k . ' as id ');
        $list = $this->db->get($this->table);
        if ($is_array)
            $list = $list->result_array();
        else
            $list = $list->result();
        return $list;
    }

    function base_update($id, $data)
    {
        if ($id == null || $id == "") return false;
        if ($this->p_k == null || $this->p_k == "" || $this->table == null || $this->table == "") return false;
        $this->db->where($this->p_k, $id);
        $this->db->update($this->table, $data);
    }

    function base_add($data)
    {
        $this->db->insert($this->table, $data);
        $id = $this->db->insert_id();
        return $id;
    }

    function base_delete($id, $is_drop = false)
    {
        if ($id == null || $id == "") return false;
        if ($this->p_k == null || $this->p_k == "" || $this->table == null || $this->table == "") return false;
        $this->db->where($this->p_k, $id);
        if ($is_drop) {
            $this->db->delete($this->table);
            return true;
        } else {
            if (!$this->is_state || $this->state == "" || $this->state == null) return false;
            $this->db->update($this->table, array($this->state, 3));
        }
        return false;
    }

    function get($id)
    {
        $result = $this->db->get_where($this->table, array($this->p_k => $id));
        if ($result->num_rows() == 0) return array();
        return $result->row_array();
    }

    public function _get_datatables_query($table, $column_order, $column_search, $col_where, $where, $order, $joinTable, $joinCol)
    {

        if ($column_order != null) {
            $this->db->select($column_order);
        }
        if ($joinTable != null) {
            if (is_array($joinTable)) {
                foreach ($joinTable as $key => $value) {
                    $this->db->join($joinTable[$key], $joinCol[$key]);
                }
            } else {
                $this->db->join($joinTable, $joinCol);
            }
        }
        if ($col_where != 'not') {
            if (!is_array($col_where)) {
                $this->db->where($col_where, $where);
            }
        }
        if (is_array($col_where) && is_array($where) && !empty($col_where) && !empty($where)) {
            $i= 0;
            foreach ($col_where as $item)
            {
                $this->db->where($item,$where[$i]);
                $i++;
            }
        }
        $this->db->from($table);

        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if (isset($_POST['search']['value'])) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($column_search) - 1 == $i) {//last loop
                    $this->db->group_end(); //close bracket
                }
            } else {
                break;
            }
            $i++;
        }
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function datatable()
    {
        $table = $this->table;
        $column_order = $this->column_order;
        $column_search = $this->column_search;
        $col_where = $this->col_where;
        $where = $this->where;
        $order = $this->order;
        $joinTable = $this->joinTable;
        $joinCol = $this->joinCol;

        $this->_get_datatables_query($table, $column_order, $column_search, $col_where, $where, $order, $joinTable, $joinCol);
        if (isset($_POST['length']) && $_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }


        $query = $this->db->get()->result_array();

        return $query;
    }

    public function count_filtered()
    {
        $table = $this->table;
        $column_order = $this->column_order;
        $column_search = $this->column_search;
        $col_where = $this->col_where;
        $where = $this->where;
        $order = $this->order;
        $joinTable = $this->joinTable;
        $joinCol = $this->joinCol;
        $this->_get_datatables_query($table, $column_order, $column_search, $col_where, $where, $order, $joinTable, $joinCol);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $table = $this->table;
        $column_order = $this->column_order;
        $column_search = $this->column_search;
        $col_where = $this->col_where;
        $where = $this->where;
        $order = $this->order;
        $joinTable = $this->joinTable;
        $joinCol = $this->joinCol;
        if ($col_where != 'not') {
            if (is_array($col_where)) {
                foreach ($col_where as $key => $value) {
                    if (is_array($col_where[$key])) {
                        foreach ($col_where[$key] as $key1 => $value1) {
                            if ($key1 == 0) {
                                $this->db->where($col_where[$key][$key1], $where[$key][$key1]);
                            } else {
                                $this->db->or_where($col_where[$key][$key1], $where[$key][$key1]);
                            }
                        }
                    } else {
                        $this->db->where($col_where[$key], $where[$key]);
                    }
                }
            } else {
                $this->db->where($col_where, $where);
            }
        }
        $this->db->from($table);
        if ($joinTable != null && $joinCol != null) {
            if (is_array($joinTable)) {
                foreach ($joinTable as $key => $value) {
                    $this->db->join($joinTable[$key], $joinCol[$key]);
                }
            } else {
                $this->db->join($joinTable, $joinCol);
            }
        }
        return $this->db->count_all_results();
    }


    public function _get_datatables_query_for_query($query, $limit = null, $start = null)
    {

        if (isset($limit) && !isset($start)) {
            $query = $query . ' LIMIT ' . $limit;
        } elseif (isset($limit) && isset($start)) {
            $query = $query . ' LIMIT ' . $limit . ' OFFSET ' . $start;
        }
        $data = $this->db->query($query);
        return $data;
    }


    public function get_datatables_by_query($query, $limit = null, $start = null)
    {
        $data = $this->_get_datatables_query_for_query($query, $limit, $start);
        $data = $data->result();
        return $data;
    }

    public function count_filtered_by_query($query, $limit = null, $start = null)
    {
        $query = $this->_get_datatables_query_for_query($query, $limit, $start);
        return $query->num_rows();
    }

    public function count_all_by_query($query)
    {
        $query = $this->db->query($query);
        return $query->num_rows();
    }


}