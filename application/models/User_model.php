<?php

require_once 'Base.php';

class  User_model extends  Base{

    public function __construct()
    {
        parent::__construct();
        $this->table= "users";
        $this->p_k= "u_id";
        $this->title  = 'u_name_ar';
    }

    function get_social_media($user, $social)
    {
        $result = $this->db->get_where('user_social_media', array('user' => $user, 'social_media' => $social));
        if ($result->num_rows() == 0) {
            return '';
        }
        return $result->row()->usm_value;

    }

}