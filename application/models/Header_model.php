<?php
require_once 'Base.php';

class Header_model extends Base
{
    function __construct()
    {
        parent::__construct();
        $this->table = "header";
        $this->p_k = "h_id";
        $this->title = 'head';
        $this->is_state = false;
        $this->column_order = array('h_date', 'h_url','h_title');
        $this->column_search = array();
        $this->col_where = array();
        $this->where = array();
        $this->order = array('s_id');
       
        
    }

   public function create($set){
    $this->db->insert('head', $set);
   }
  
   public function getNav(){
    $query = $this->db->get('head');
    $temp=(array)$query->result();
    
    $array_nav=array();
    foreach($temp as $value )
       {
           $value_temp=(array)$value;
           if($value_temp['parent']==null){
           
           
       $array_nav[$value_temp['id']]['data']=(array)$value;
          $list_select[$value_temp['id']]['title']=$value_temp['h_title'];
          $list_select[$value_temp['id']]['id']=$value_temp['id'];
          $array_nav[$value_temp['id'] ]['children']=null;
       foreach( $temp as $parent){
        
        $parent_temp=(array)$parent;
         if($value_temp['id'] == $parent_temp['parent']){
            $array_nav[$value_temp['id'] ]['children'][$parent_temp['parent']]=$parent_temp;
         }
       }
       
    }
   }
   $data['list_select']=$list_select;
   $data['nav']=$array_nav;
  return  $data;
}
public function getList(){

}
public function destroy($id){
  
   $this->db->where('id', $id);
$this->db->delete('mytable');
}
  
}