<?php

function translate($word)
{
    $CI = get_instance();

    $value = $CI->db->get_where('word', array('key' => $word));
//    echo  $CI->db->last_query();
//    echo  $value->num_rows() ;
    if ($value->num_rows() != 0) {

        if (!isset($_SESSION['language'])) {
            $_SESSION['language'] = 'arabic';
        }

        $value = $value->row_array();

        return $value[$_SESSION['language']];
    }
    else {
        $data['key'] = $word;
        $data['arabic'] = $word;
        $data['english'] = $word;
        $CI->db->insert('word', $data);
        return $word;
    }
}

function send_email($title, $message, $to, $from = 'مركز الدعم', $bcc = array(), $cc = null)
{

    $CI = get_instance();
    $CI->load->library('email');
    $config = array();
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'mail.easy-order-plus.com';
    $config['smtp_user'] = 'support@easy-order-plus.com';
    $config['smtp_pass'] = 'a12345678/Toz';
    $config['mailtype'] = 'html';
    $config['charset'] = 'utf-8';
    $config['wordwrap'] = true;
    $config['smtp_port'] = 26;
    $CI->email->initialize($config);
    $CI->email->from('support@easy-order-plus.com', $from);
    $CI->email->to($to);
    if (!empty($bcc))
        $CI->email->bcc($bcc);
    if (!empty($cc))
        $CI->email->bcc($cc);
    $CI->email->subject($title);
    $CI->email->message($message);
    $CI->email->send();
}

function create_form_group($title, $name, $id, $placeholder = "", $type = "text", $value = "", $width = 6, $attr = "")
{
    echo '<div  class="col-md-' . $width . ' col-sm-12">';
    echo '<div class="form-group">';
    echo "<label  for='" . $id . "'>" . translate($title) . "</label>";
    echo "<input style='border-radius:0'   " . $attr . " class='form-control' name='" . $name . "' value='" . $value . "' placeholder='" . translate($placeholder) . "'   id='" . $id . "'  type='" . $type . "'>";
    echo "<b class='text-danger' id='error_" . $id . "'> </b>";
    echo '<style>.bootstrap-tagsinput{width:100%}</style>';
    echo "</div>";
    echo "</div>";
}

function create_form_group3($title, $name, $id, $placeholder = "", $type = "text", $value = "", $width = 6, $attr = "")
{
    echo '<style>.bootstrap-tagsinput{width:100%}</style>';
    echo '<div  class="col-md-' . $width . ' col-sm-12">';
    echo '<div class="form-group row">';
    echo '<div class="col-md-2 col-md-offset-1">';
    echo "<label for='" . $id . "'>" . translate($title) . "</label>";
    echo "</div>";
    echo '<div class="col-md-8">';
    echo "<input style='border-radius:0'   " . $attr . " class='form-control ' name='" . $name . "' value='" . $value . "' placeholder='" . translate($placeholder) . "'   id='" . $id . "'  type='" . $type . "'>";
    echo "<b class='text-danger '   id='error_" . $id . "'>   </b>";

    echo "</div>";
    echo "</div>";
    echo "</div>";
}


function create_form_group2($title, $name, $id, $placeholder = "", $type = "text", $value = "", $width = 6, $attr = "")
{
    echo '<div class="form-group" style="width: 150px">';
    echo "<input   " . $attr . " class='form-control' name='" . $name . "' value='" . $value . "' placeholder='" . translate($placeholder) . "'   id='" . $id . "'  type='" . $type . "'>";
    echo "<b class='text-danger' id='error_" . $id . "'> </b>";
    echo '<style>.bootstrap-tagsinput{width:100%}</style>';
    echo "</div>";
}

function create_tag($title, $name, $id, $placeholder = "", $value = "")
{
    echo '<div class="col-md-12 col-sm-12">';
    echo '<div class="form-group">';
    echo "<label  for='" . $id . "'>" . translate($title) . "</label><br>";
    echo '<input tabindex="-1" class="form-control" style="width:100%" id="' . $id . '"  name="' . $name . '" placeholder="' . $placeholder . '" type="text" data-role="tagsinput" />';
    echo "<b class='text-danger' id='error_" . $id . "'> </b>";
    echo "</div>";
    echo "</div>";
}

function create_form_group_editor($title, $name, $id, $placeholder = "", $value = "", $width = 12)
{
    echo '<div class="col-md-' . $width . ' col-sm-12">';
    echo '    <div class="form-group">';
    echo "<label  for='" . $id . "'>" . translate($title) . "</label>";
    echo "<textarea class='summernote'  name='" . $name . "'  placeholder='" . translate($placeholder) . "'   id='" . $id . "'  >" . $value . "</textarea>";
    echo "<b class='text-danger' id='error_" . $id . "'> </b>";
    echo "    </div>";
    echo "</div>";
    echo "<script>
$(document).ready(function() {
//    debugger;
  $('#".$id."').summernote();
});
</script>";

}

function create_check_box($title, $name, $id, $checked = false, $width = 6)
{
    if ($checked) {
        echo $checked = "checked";
    }
    else {
        $checked = "";
    }
    echo '<div class="col-md-' . $width . ' col-sm-12" >';
    echo '    <div style="padding: 3px" class="form-group form-check" >
                <input  type="checkbox" ' . $checked . ' class="form-check-input" id="' . $id . '" name="' . $name . '">
                <label class="form-check-label" for="' . $id . '">' . $title . '</label>
            </div>
            </div>
            
            ';
}

function create_select22($title, $name, $id, $data = array(), $data_title, $data_value, $placeholder = "", $value = false, $muti = false, $width = 6)
{
    if ($muti) {
        $muti = 'multiple';
        $name = $name . '[]';
    }
    else {
        $muti = "";
    }

    // for data , you should get from result_array .
//print_r($data);
    echo '<div class="form-group" style="width: 150px">';
    echo '<select ' . $muti . '   class="form-control" name="' . $name . '" id="' . $id . '">';

    foreach ($data as $item) {
        if ($muti == "")
            echo '<option  value="' . $item[$data_value] . '">' . $item[$data_title] . '</option>';
        else {
            echo '<option   value="' . $item[$data_value] . '">' . $item[$data_title] . '</option>';

        }
    }
    echo '</select>';
    echo "<b class='text-danger' id='error_" . $id . "'> </b>";
    echo "</div>";
    if ($muti == "")
        echo '
<script>
    $("#"+"' . $id . '").select2({
    theme:"bootstrap",
    }); 
    $("#' . $id . '").val( ' . $value . ').trigger("change");
</script>';
    else
        echo '
<script>
    $("#"+"' . $id . '").select2({
    theme:"bootstrap",
    }); 
    $("#' . $id . '").val( ' . json_encode($value) . ').trigger("change");
</script>';

}

function create_select2($title, $name, $id, $data = array(), $data_title, $data_value, $placeholder = "", $value = false, $muti = false, $width = 6)
{
    if ($muti) {
        $muti = 'multiple';
    }
    else {
        $muti = "";
    }

    // for data , you should get from result_array .
    echo '<div class="col-md-' . $width . ' col-sm-12">';
    echo '<div class="form-group">';
    echo "<label  for='" . $id . "'>" . translate($title) . "</label><br>";
    echo '<select ' . $muti . '   class="form-control" name="' . $name . '" id="' . $id . '">';
    foreach ($data as $item) {
        echo '<option value="' . $item[$data_value] . '">' . $item[$data_title] . '</option>';
    }
    echo '</select>';
    echo "<b class='text-danger' id='error_" . $id . "'> </b>";
    echo "</div>";
    echo "</div>";
    echo '
<script>
    $("#"+"' . $id . '").select2({
    theme:"bootstrap"
    });
        $("#"+"' . $id . '").val( ' . $value . ');
            $("#"+"' . $id . '").trigger("change");
</script>';
}

function create_select23($title, $name, $id, $data = array(), $data_title, $data_value, $placeholder = "", $value = false, $muti = false, $width = 6)
{
    if ($muti) {
        $muti = 'multiple';
    }
    else {
        $muti = "";
    }

    // for data , you should get from result_array .
    echo '<div class="col-md-' . $width . ' col-sm-12">';

    echo '<div class="form-group row">';
    echo '<div class="col-sm-12 col-md-2  col-md-offset-1">';
    echo "<label  for='" . $id . "'>" . translate($title) . "</label><br>";
    echo "</div>";
    echo '<div class="col-sm-12 col-md-8">';
    echo '<select ' . $muti . '   class="form-control" name="' . $name . '" id="' . $id . '">';
    foreach ($data as $item) {
        echo '<option value="' . $item[$data_value] . '">' . $item[$data_title] . '</option>';
    }
    echo '</select>';
    echo "<b class='text-danger' id='error_" . $id . "'> </b>";
    echo "</div>";
    echo "</div>";
    echo "</div>";


    echo '
<script>
    $("#"+"' . $id . '").select2({
    theme:"bootstrap"
    });
        $("#"+"' . $id . '").val( ' . $value . ');
            $("#"+"' . $id . '").trigger("change");
</script>';
}