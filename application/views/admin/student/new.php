<div class="container">
    <div class="row">
        <br>
        <br>
        <h3>New Student</h3>
        <hr>

        <div class="col-md-8 col-md-offset-2">
            <form id="form">
                <div class="row">
                    <?php
                    //user information - step 1
                    require __DIR__ . '/../create_user.php';
                    //training center  - step 2
                    create_form_group('birthday', 'birthday', 'birthday', 'birthday', 'date');
                    create_form_group('facebook url', 'facebook', 'facebook', 'https://facebook.com/student_profile', 'text');
                    create_form_group_editor('More Information', 'about', 'about', 'More Information');
                    ?>
                    <div class="col-md-12">
                        <button class="btn btn-primary" id="btn-submit"><?= translate('Save') ?></button>

                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.summernote').summernote();
        $("#form").submit(function (event) {
            event.preventDefault();
            $("#btn-submit").attr('disabled', 'disabled').html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i> <?=translate("loading")?>');
                $.ajax({
                url: "<?=base_url('admin/students/add')?>",
                method: "post",
                data: $(this).serialize(),
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.result == 0) {
                        $("#error_name_ar").html(data.name_ar);
                        $("#error_name_en").html(data.name_en);
                        $("#error_email").html(data.email);
                        $("#error_mobile").html(data.mobile);
                        $("#error_countries").html(data.countries);
                        $("#error_city").html(data.city);
                        $("#error_gender").html(data.gender);
                        $("#btn-submit").removeAttribute('disabled').html('<?=translate("Save")?>');
                    } else if (data.result == 1) {
                        $("#btn-submit").html('<?=translate("Done")?>');
                        setTimeout(function () {
                            window.location.href = "<?=base_url('admin/students')?>"
                        }, 3000);
                    }
                }
            });
        });
    });
</script>