<div class="col-md-12">
    <div class="table-responsive" style="overflow-x:auto;">

        <table class="table">
            <thead>
            <th><?= translate('name arabic') ?></th>
            <th><?= translate('name english') ?></th>
            <th><?= translate('mobile') ?></th>
            <th><?= translate('telephone') ?></th>
            <th><?= translate('email') ?></th>
            <th><?= translate('gender') ?></th>
            <th><?= translate('birthday') ?></th>
            <th><?= translate('facebook') ?></th>
            <th><?= translate('country') ?></th>
            <th><?= translate('city') ?></th>
            <th><?= translate('Option') ?></th>
            </thead>
            <tbody>
            <?php
            $genders = $this->db->select('g_id as id , g_title as text')->get('gender')->result_array();
            $i = 0;
            $gender = 0;
            foreach ($data as $item) {
                $i++;
                $countries = $this->db->select('c_name as text , c_id as id ')->get('countries')->result_array();
                $country = $this->db->select('c_name as text , c_id as id ')->like('c_name', $item['country'])->get('countries')->result_array();

                if (count($country) > 0) {
                    $country = $country[0]['id'];
                } else {
                    $country = null;
                }
                $cities = $this->db->select('c_name as text , c_id as id ')->get('city')->result_array();
                $city = $this->db->select('c_name as text , c_id as id ')->like('c_name', $item['city'])->get('city')->result_array();

                if (count($city) > 0) {
                    $city = $city[0]['id'];
                } else {
                    $city = null;
                }

                if ($item['gender'] == "man" || $item['gender'] == "male" || $item['gender'] == 'ذكر') {
                    $gender = 1;
                } else {

                    $gender = 2;
                }
                ?>


                <tr id="row_<?= $i ?>">

                    <td><?php create_form_group2('name arabic', 'name_ar', 'name_ar_' . $i, 'name arabic', 'text', $item['name_ar']); ?>                </td>
                    <td><?php create_form_group2('name english', 'name_en', 'name_en_' . $i, 'name english', 'text', $item['name_en']); ?>              </td>
                    <td><?php create_form_group2('mobile', 'mobile', 'mobile_' . $i, 'mobile', 'text', $item['mobile']); ?>                             </td>
                    <td><?php create_form_group2('telephone', 'telephone', 'telephone_' . $i, 'telephone', 'text', $item['telephone']); ?>              </td>
                    <td><?php create_form_group2('email', 'email', 'email_' . $i, 'email', 'email', $item['email']); ?>                                 </td>
                    <td><?php create_select22('gender', 'gender', 'gender_' . $i, $genders, 'text', 'id', 'gender', $gender); ?>            </td>
                    <td>  <?php create_form_group2('birthday', 'birthday', 'birthday_' . $i, 'birthday', 'date', $item['birthday']); ?>             </td>
                    <td>     <?php create_form_group2('facebook', 'facebook', 'facebook_' . $i, 'facebook', 'url', $item['facebook']); ?>              </td>
                    <td><?php create_select22('country', 'country', 'country_' . $i, $countries, 'text', 'id', 'country', $country); ?>     </td>
                    <td><?php create_select22('city', 'city', 'city_' . $i, $cities, 'text', 'id', 'city', $city); ?>                       </td>
                    <td>
                        <button type="button" onclick="submit_function(<?= $i ?>)" id="btn_submit_<?= $i ?>"
                                class="btn btn-primary"><i class="fa fa-check"></i></button>
                    </td>

                </tr>


                <script>

                    function submit_function(id) {

                        var formData = new FormData();
                        formData.append('name_ar', $("#name_ar_" + id).val());
                        formData.append('name_en', $("#name_en_" + id).val());
                        formData.append('mobile', $("#mobile_" + id).val());
                        formData.append('telephone', $("#telephone_" + id).val());
                        formData.append('email', $("#email_" + id).val());
                        formData.append('gender', $("#gender_" + id).val());
                        formData.append('birthday', $("#birthday_" + id).val());
                        formData.append('country', $("#country_" + id).val());
                        formData.append('city', $("#city_" + id).val());
                        $("#btn_submit_" + id).html('<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>').attr('disabled', 'disabled');
                        $.ajax({
                            url: "<?=base_url('admin/students/accepte_import')?>",
                            data: formData,
                            method: "post",
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false,
                            success: function (response) {
                                var data = JSON.parse(response);
                                if (data.result == 1) {
                                    $("#btn_submit_" + id).html('<?=translate("Done")?>');
                                    setTimeout(function () {
                                        $("#row_" + id).hide();
                                    }, 3000)
                                } else {
                                    $("#error_name_ar_" + id).html(data.name_ar);
                                    $("#error_name_en_" + id).html(data.name_en);
                                    $("#error_mobile_" + id).html(data.mobile);
                                    $("#error_telephone_" + id).html(data.telephone);
                                    $("#error_email_" + id).html(data.email);
                                    $("#error_gender_" + id).html(data.gender);
                                    $("#error_birthday_" + id).html(data.birthday);
                                    $("#error_country_" + id).html(data.country);
                                    $("#error_city_" + id).html(data.city);
                                    $("#btn_submit_" + id).html('<i class="fa fa-check"></i>').removeAttr('disabled');

                                }

                            }
                        });
                    }
                </script>
                <?php
            } ?>
            </tbody>
        </table>
    </div>
</div>
