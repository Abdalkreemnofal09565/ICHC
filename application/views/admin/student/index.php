<div class="container" style="h">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-md-6">
                        <h3 style="color: #00A7E5"><?= translate('student table') ?></h3>

                    </div>
                    <div class="col-md-6 text-right">
                        <a href="<?=base_url('admin/students/new')?>" class="btn btn-primary"><i class="fa fa-plus"></i><?=translate('new student')?></a>
                        <a href="<?=base_url('admin/students/import')?>" class="btn btn-primary"><i class="fa fa-file-excel-o"></i><?=translate('Import By Excel')?></a>
                    </div>
                </div>
       <div class="row">
           <div class="col-md-12">

               <hr>
               <table class="table">
                   <thead>
                   <th><?= translate('#') ?></th>
                   <th><?= translate('arabic name') ?></th>
                   <th><?= translate('english name') ?></th>
                   <th><?= translate('email') ?></th>
                   <th><?= translate('phone') ?></th>
                   <th><?= translate('telephone') ?></th>
                   <th><?= translate('country') ?></th>
                   <th><?= translate('city') ?></th>
                   <th><?= translate('state') ?></th>
                   <th><?= translate('option') ?></th>
                   </thead>
               </table>
               <br>

               <br>
           </div>
       </div>
            </div>
        </div>
    </div>
</div>

<script>
    var table;
    $(document).ready(function () {
        table = $('.table').dataTable({
            "oLanguage": {
                "sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ordering": false,
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=$datatable?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                }, {
                    "data": "name_ar"
                }, {
                    "data": "name_en"
                }, {
                    "data": "email"
                }, {
                    "data": "mobile"
                }, {
                    "data": "telephone"
                }, {
                    "data": "country_name"
                }, {
                    "data": "city_name"
                }, {
                    "data": "state"
                }, {
                    "data": "option"
                },

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    });

</script>