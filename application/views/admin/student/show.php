<div class="container">
    <div class="row">
        <div class="col-md-12" style="padding: 25px">

            <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr class="btn-primary">
                            <td colspan="2" class="text-center" style="text-transform: uppercase">
                                <b>
                                    <?= translate('student Information') ?>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td><?= translate('name arabic') ?> : <?= $user['u_name_ar'] ?></td>
                            <td><?= translate('name english') ?> : <?= $user['u_name_en'] ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?= translate('email') ?> : <?= $user['u_email'] ?></td>
                        </tr>
                        <tr>
                            <td><?= translate('mobile') ?> : <?= $user['u_mobile'] ?></td>
                            <td><?= translate('telephone') ?> : <?= $user['u_telephone'] ?></td>
                        </tr>
                        <tr>
                            <td><?= translate('gender') ?> :
                                <?php if ($user['gender'] == 1) echo translate('man'); else echo translate('woman') ?>
                            </td>
                            <td><?= translate('birtthday') ?> : <?= $student['s_birthdate'] ?></td>
                        </tr>
                        <tr>
                            <td><?= translate('country') ?> :
                                <?php
                                echo $this->db->get_where('countries', array('c_id' => $user['country']))->row()->c_name;
                                ?>
                            </td>
                            <td><?= translate('city') ?> :
                                <?php
                                echo $this->db->get_where('city', array('c_id' => $user['city']))->row()->c_name;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><?= translate('facebook url') ?> : <a
                                        href=" <?= $student['s_facebook'] ?>"> <?= $student['s_facebook'] ?></a></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?= translate('about') ?>
                                <?= $student['s_additional_information'] ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
    <div class="row">
        <br>
        <h3><?= translate('all courses') ?>
        </h3>
        <hr>
        <?php
        $courses = $this->db->where('student', $student['s_id'])->join('course', 'course.c_id=course_student.course')->get('course_student')->result_array();
        foreach ($courses as $item) {
            $city =  $this->db->get_where('city', array('c_id' => $item['city']))->row();
            ?>

            <div class="row ">
                <div class="col-md-12 table-responsive">
                    <table class="table bg-grey table-bordered">
                        <tr class="bg-dark text-center">
                            <td colspan="2"><h4><b style="color:#fff"><?= $item['c_title'] ?></b></h4></td>
                        </tr>
                        <tr>
                            <td><?= translate('country') ?> :
                                <?php
                                echo $this->db->get_where('countries', array('c_id' => $city->country_id))->row()->c_name;
                                ?>
                            </td>
                            <td><?= translate('city') ?> :
                               <?=$city->c_name?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= translate('location') ?> :
                                <?= $item['c_location'] ?>
                            </td>
                            <td>
                                <?= translate('cost') ?> :
                                <?= $item['c_cost'] ?> <b class="text-primary">$</b>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <?=translate('count_hours')?> :
                                <?=$item['c_count_hours']?> <b class="text-primary"><?=translate('Hours')?></b>
                            </td>
                            <td>
                                <?=translate('mark')?> :
                               <b> <?=$item['cs_examـmark']?></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><?=translate('Maters')?>
                             :</td>
                        </tr>
                        <tr>
                            <td colspan="2"><?=translate('Teachers')?>
                             :</td>
                        </tr>           <tr>
                            <td colspan="2"><?=translate('training centers')?>
                             :</td>
                        </tr>
                    </table>
                </div>
            </div>

            <?php
        }
        ?>
    </div>
</div>
