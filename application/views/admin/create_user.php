<?php
if (isset($user)) {
    $email = $user['u_email'];
    $name_ar = $user['u_name_ar'];
    $name_en = $user['u_name_en'];
    $mobile = $user['u_mobile'];
    $telephone = $user['u_telephone'];
    $u_gender = $user['gender'];
    $u_country = $user['country'];
    $u_city = $user['city'];

} else {
    $email = "";
    $name_ar = "";
    $name_en = "";
    $mobile = "";
    $telephone = "";
    $u_gender = "";
    $u_country = "";
    $u_city = "";
}

create_form_group("Name english", "name_ar", 'name_ar', 'Name english', 'text', $name_ar);
create_form_group("Name Arabic", "name_en", 'name_en', 'Name Arabic', 'text', $name_en);
create_form_group("Email", "email", 'email', 'Email', 'text', $email, '12');
create_form_group("mobile", "mobile", 'mobile', 'mobile', 'text', $mobile);
create_form_group("telephone", "telephone", 'telephone', 'telephone', 'text', $telephone);
$gender = $this->db->get('gender')->result_array();
create_select2('gender', 'gender', 'gender', $gender, 'g_title', 'g_id', 'gender', $u_gender);
$country = $this->db->get('countries')->result_array();
create_select2('countries', 'country', 'country', $country, 'c_name', 'c_id', 'countries', $u_country);
$city = $this->db->select('c_name , c_id ')->get('city')->result_array();
create_select2('city', 'city', 'city', $city, 'c_name', 'c_id', 'city', $u_city);

?>
<script>
    $(document).ready(function () {
        $("#country").change(function () {
            $("#city").select2({
                theme: "bootstrap",
                ajax: {
                    url: "<?=base_url('pages/city_list/')?>" + $("#country").val(),
                    dataType: 'json',
                    type: "GET",
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });
        });
        <?php if (isset($user)) {
        ?>

        $("#city").select2({
            theme: "bootstrap",
            ajax: {
                url: "<?=base_url('pages/city_list/' . $u_country)?>",
                dataType: 'json',
                type: "GET",
                processResults: function (data) {

                    return {
                        results: data,
                        success: function () {
                            $("#city").val("<?=$u_city?>");
                            $("#city").trigger("change");
                        }
                    };
                }
            },

        });


        <?php
        } ?>

    });
</script>

