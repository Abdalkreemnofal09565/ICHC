<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-md-6">
                        <h3 style="color: #00A7E5"><?= translate('teachers table') ?></h3>

                    </div>
                    <div class="col-md-6 text-right">
                        <a href="<?=base_url('admin/teachers/new')?>" class="btn btn-primary"><i class="fa fa-plus"></i><?=translate('new teachers')?></a>
                        <a href="<?=base_url('admin/teachers/import')?>" class="btn btn-primary"><i class="fa fa-file-excel-o"></i><?=translate('Import By Excel')?></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <hr>
                        <table class="table">
                            <thead>
                            <th><?= translate('#') ?></th>
                            <th><?= translate('arabic name') ?></th>
                            <th><?= translate('english name') ?></th>
                            <th><?= translate('email') ?></th>
                            <th><?= translate('phone') ?></th>
                            <th><?= translate('telephone') ?></th>
                            <th><?= translate('country') ?></th>
                            <th><?= translate('city') ?></th>
                            <th><?= translate('certified') ?></th>
                            <th><?= translate('certificate training') ?></th>
                            <th><?= translate('previously trained') ?></th>
                            <th><?= translate('training out country') ?></th>
                            <th><?= translate('training out city') ?></th>
                            <th><?= translate('price hour') ?></th>
                            <th><?= translate('state') ?></th>
                            <th><?= translate('option') ?></th>
                            </thead>
                        </table>
                        <br>

                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var table;
        var name_table = "admin/teachers";
    $(document).ready(function () {
        table = $('.table').dataTable({
            "oLanguage": {
                "sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ordering": false,
            "scrollX": true,
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=$datatable?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                }, {
                    "data": "name_ar"
                }, {
                    "data": "name_en"
                }, {
                    "data": "email"
                }, {
                    "data": "mobile"
                }, {
                    "data": "telephone"
                }, {
                    "data": "country_name"
                }, {
                    "data": "city_name"
                },  {
                   "data": "certified"
                },  {
                   "data": "certificate_training"
                },  {
                   "data": "previously_trained"
                },  {
                    "data": "training_out_country"
                },  {
                    "data": "training_out_city"
                }, {
                    "data": "price"
                }, {
                    "data": "state"
                },{
                    "data": "option"
                },

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
    });
</script>