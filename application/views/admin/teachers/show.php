<div class="container">
    <article>

        <div class="post-content">
            <div class="post-title">
                <div class="col-md-2">
                    <img src="<?= base_url('user.png') ?>" width="100" alt="">
                </div>
                <div class="col-md-9">
                    <h1><?= $user['u_name_ar'] . ' - ' . $user['u_name_en'] ?></h1>
                </div>
            </div>
            <div class="post-metadata">
                                        <span class="posted-on">
                                            <i class="fa fa-clock-o"></i>
                                           <?= $user['u_date_register'] ?>
                                        </span>

                <span class="cat-links">
                                            <i class="fa fa-folder-open"></i>
                                            <a href="#">
                                                <?= count($courses) ?>
                                                <?= translate('Courses') ?></a>
                                        </span>
                <span class="comment-link">
                                            <i class="fa fa-heart-o"></i>
                                            <a href="#">29 <?= translate('Rate') ?></a>
                                        </span>
                <div class="social-share pull-right">
                    <a href="<?= '@' ?>"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-at"></i></a>
                    <a href="#"><i class="fa fa-phone"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
            <div class="footer-entry clearfix">
                <?= $teacher['t_additional_information'] ?>
            </div>
            <div class="post-entry">
                <div class="row">
                    <h2 class="box-title"> Features</h2>
                    <?php
                    if ($teacher['t_certificate_training'] == 1) {
                        ?>
                        <div class="col-md-6 col-sm-4">
                            <div class="feature-box">


                                <div class="feature">
                                    <h3><br>    <?=translate('certificate training')?></h3>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                    <?php
                    if ($teacher['t_certificate_appreciation'] == 1) {
                        ?>
                        <div class="col-md-6 col-sm-4">
                            <div class="feature-box">


                                <div class="feature">
                                    <h3><br><?=translate('certificate appreciation')?></h3>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                    <?php
                    if ($teacher['t_previously_trained'] == 1) {
                        ?>
                        <div class="col-md-6 col-sm-4">
                            <div class="feature-box">


                                <div class="feature">
                                    <h3><br><?=translate('previously trained')?></h3>
                                    <i class="fa fa-star"></i>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                    <?php
                    if ($teacher['t_training_out_country'] == 1) {
                        ?>
                        <div class="col-md-6 col-sm-4">
                            <div class="feature-box">


                                <div class="feature">
                                    <h3><br><?=translate('training out country')?></h3>
                                    <i class="fa fa-map-marker"></i>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                    <?php
                    if ($teacher['t_training_out_city'] == 1) {
                        ?>
                        <div class="col-md-6 col-sm-4">
                            <div class="feature-box">


                                <div class="feature">
                                    <h3><br><?=translate('training out city')?></h3>
                                    <i class="fa fa-home"></i>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                    <?php
                    if ($teacher['tـcertified'] == 1) {
                        ?>
                        <div class="col-md-6 col-sm-4">
                            <div class="feature-box">


                                <div class="feature">
                                    <h3><br><?=translate('certified')?></h3>
                                    <i class="fa fa-check"></i>
                                </div>

                            </div>
                        </div>
                    <?php } ?>

                </div>
                <br>
            </div>


        </div>
    </article>
</div>