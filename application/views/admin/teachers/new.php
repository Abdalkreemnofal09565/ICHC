<div class="container">
    <div class="row">
        <br>
        <br>
        <h3><?= translate('New teacher') ?></h3>
        <hr>

        <div class="col-md-8 col-md-offset-2">
            <form id="form">
                <div class="row">
                    <?php
                    //user information - step 1
                    require __DIR__ . '/../create_user.php';
                    //TEACHERS  - step 2
                    create_form_group('birthdate', 'birthdate', 'birthdate', 'birthdate', 'date', "");
                    create_form_group('price hour', 'price_hour', 'price_hour', 'price hour(USD `$` )', 'number', "",12);
                    create_check_box('certificate training','certificate_training','certificate_training');
                    create_check_box('certificate appreciation','certificate_appreciation','certificate_appreciation');
                    create_check_box('previously trained','previously_trained','previously_trained');
                    create_check_box('training out country','training_out_country','training_out_country');
                    create_check_box('training out city','training_out_city','t_training_out_city');
                    create_check_box('certified','certified','tـcertified');
                    create_form_group_editor('More information', 'additional_information', 'additional_information', 'additional information');
                   ?>
                    <div class="col-md-12">
                        <button class="btn btn-primary" id="btn-submit"><?= translate('Save') ?></button>

                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.summernote').summernote();
        $("#form").submit(function (event) {
            event.preventDefault();
            $("#btn-submit").attr('disabled', 'disabled').html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i> <?=translate("loading")?>');
            $.ajax({
                url: "<?=base_url('admin/teachers/add')?>",
                method: "post",
                data: $(this).serialize(),
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.result == 0) {
                        $("#error_name_ar").html(data.name_ar);
                        $("#error_name_en").html(data.name_en);
                        $("#error_email").html(data.email);
                        $("#error_mobile").html(data.mobile);
                        $("#error_countries").html(data.countries);
                        $("#error_city").html(data.city);
                        $("#error_gender").html(data.gender);
                        $("#btn-submit").removeAttribute('disabled').html('<?=translate("Save")?>');
                    } else if (data.result == 1) {
                        $("#btn-submit").html('<?=translate("Done")?>');
                        setTimeout(function () {
                            window.location.href = "<?=base_url('admin/teachers')?>"
                        }, 3000);
                    }
                }
            });
        });
    });
</script>