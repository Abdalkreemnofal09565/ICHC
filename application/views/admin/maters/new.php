<div class="container">
    <div class="row">
        <br>
        <br>
        <h3>New Mater</h3>
        <hr>

        <div class="col-md-8 col-md-offset-2">
            <form id="form">
                <div class="row">
                    <?php
                    create_form_group("title maters", "title", 'title', 'title mater', 'text');
                    create_form_group("count hours", "hours", 'hours', 'count hours', 'number');
                    create_check_box(' is certified ? ', 'certified', 'certified', false);
                    create_form_group_editor("about maters", "about", 'about', 'about mater');
                    create_form_group_editor("scientific content", "scientific_content", 'scientific_content', 'scientific content');
                    create_form_group_editor("scientific requirement", "scientific_requirement", 'scientific_requirement', 'scientific requirement');
                    create_tag("categories", "categories", 'categories', 'categories');
                    create_form_group_editor("more information", "more_info", 'more_info', 'more information');
                    create_form_group_editor("references", "references", 'references', 'references');

                    ?>
                    <div class="col-md-12">
                        <button class="btn btn-primary"><?= translate('save') ?></button>
                        <br>
                        <br>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.summernote').summernote();
        $("#form").submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: "<?=base_url('admin/maters/add')?>",
                method: "post",
                data: $(this).serialize(),
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.result == 0) {
                        $("#error_title").html(data.title);
                        $("#error_hours").html(data.hours);
                        $("#error_about").html(data.about);
                    }
                }
            });
        });
    });
</script>