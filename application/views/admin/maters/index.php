<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-md-6">
                        <h3 style="color: #00A7E5"><?= translate('Maters table') ?></h3>

                    </div>
                    <div class="col-md-6 text-right">
                        <a href="<?=base_url('admin/maters/new')?>" class="btn btn-primary"><i class="fa fa-plus"></i><?= translate('new Mater') ?></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <hr>
                        <table class="table">
                            <thead>
                            <th><?= translate('#') ?></th>
                            <th><?= translate('Mater name') ?></th>
                            <th><?= translate('State') ?></th>
                            <th><?= translate('Date Added') ?></th>
                            <th><?= translate('option') ?></th>
                            </thead>
                        </table>
                        <br>

                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var table;
    $(document).ready(function () {
        table = $('.table').dataTable({
            "oLanguage": {
                "sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ordering": false,
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=$datatable?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                }, {
                    "data": "title"
                }, {
                    "data": "state"
                } ,{
                    "data": "date_added"
                }, {
                    "data": "option"
                },

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
    });
</script>