<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 text-left">
                    <h3><?= translate('Import By Excel') ?></h3>
                </div>
                <div class="col-md-6 text-right">
                    <a target="_blank" href="<?= base_url('admin/course/download_template') ?>"
                       class="btn btn-primary"> <?= translate('Download Template') ?> <i class="fa fa-download"></i></a>
                </div>
            </div>
            <hr>
            <div class="row bg-grey" style="padding: 15px">
                <form id="upload_file" enctype="multipart/form-data">
                    <?php create_form_group('please chose your file', 'file', 'file', null, 'file', null, 12, '" required accept=".xls, .xlsx" '); ?>
                    <div class="col-md-12">
                        <button id="btn_upload" class="btn btn-success" style="width: 100%"><?= translate('Upload') ?>
                            <i
                                    class="fa fa-upload"></i></button>
                    </div>
                </form>

            </div>
            <hr>
            <div class="row" id="body"></div>

        </div>
    </div>
</div>
<script>
  $(document).ready(function () {
      $("#upload_file").submit(function (event) {
          event.preventDefault();
          $("#btn_upload").html('<i class="fa fa-cog fa-spin fa-1x fa-fw"></i><?=translate("in process")?>...')
          $.ajax({
              url: "<?=base_url('admin/course/do_import')?>",
              method: "POST",
              data: new FormData(this),
              contentType: false,
              cache: false,
              processData: false,
              success: function (response) {
                  $("#body").html(response);
                  $("#btn_upload").html('<?= translate("Upload") ?> <i class="fa fa-upload"></i>');
              }
          });
      });
      $(".form_student").submit(function (event) {
          event.preventDefault();
          var id = this.id;
          alert(id);
      })
  });
</script>