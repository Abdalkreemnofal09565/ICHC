<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3><?= translate('add new Students to course') ?>
                <sup>(
                    <?php
                    $title = $this->db->get_where('course', array('c_id' => $id))->row()->c_title;
                    echo $title;
                    ?>)
                </sup>
            </h3>

            <br>
        </div>

        <div class="col-md-12">
            <form id="form">
                <input type="hidden" name="course" value="<?= $id ?>">
                <div class="row bg-grey  ">
                    <div class="col-md-9  ">
                        <div class="form-group">
                            <br>
                            <select class="form-control input-lg" name="student" id="">
                                <?php foreach ($students as $item) { ?>
                                    <option id="" value="<?= $item['id'] ?>"><?= $item['text'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <br>
                        <button id="btn_added" class="btn btn-primary btn-sm" style="width: 100%"><i
                                    class="fa fa-plus"></i><?= translate('add') ?></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12">

            <hr>
            <table class="table text-center">
                <thead>
                <th class=" text-center"><?= translate('#') ?></th>
                <th class=" text-center"><?= translate('Name Arabic') ?></th>
                <th class=" text-center"><?= translate('Name English') ?></th>
                <th class=" text-center"><?= translate('Exam mark') ?></th>
                <th class=" text-center"><?= translate('Mobile') ?></th>
                <th class=" text-center"><?= translate('Email') ?></th>
                <th class=" text-center"><?= translate('telephone') ?></th>
                <th class=" text-center"><?= translate('Date Added') ?></th>
                <th class=" text-center"><?= translate('option') ?></th>
                </thead>
            </table>
            <br>

            <br>
        </div>

    </div>
</div>
<div class="modal fade" id="modal_mark" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="add_mark">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= translate('set mark') ?><sub id="student_name" class="text-warning">(student_name)</sub>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="student_id" value="" id="student_id">
                        <?php create_form_group('mark', 'mark', 'mark', 'mark', 'number', null, '12', "max='100' maxlength='3'  "); ?>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger"
                            data-dismiss="modal"><?= translate('Close') ?></button>
                    <button type="submit" class="btn btn-primary"><?= translate('save') ?></button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function set_mark(id) {
        $.ajax({
            url: "<?=base_url('admin/course/students/get_mark')?>",
            method: "post",
            data: {id: id},
            success: function (response) {
                var data = JSON.parse(response);
                if (data.result == 1) {
                    $("#modal_mark").modal('show');
                    // student name + id + mark
                    $("#student_name").text(data.student_name);
                    $("#student_id").val(id);
                    $("#mark").val(data.mark);
                } else {
                    alert("<?=translate('please try again')?>");
                }
            }
        })
    }

    $(document).ready(function () {
        var table;
        table = $('.table').dataTable({
            "oLanguage": {
                "sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ordering": false,
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=$datatable?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                }, {
                    "data": "name_ar"
                }, {
                    "data": "name_en"
                }, {
                    "data": "examـmark"
                }, {
                    "data": "mobile"
                }, {
                    "data": "email"
                }, {
                    "data": "telephone"
                }, {
                    "data": "date_added"
                }, {
                    "data": "option"
                },

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
        $("select").select2({
            theme: "bootstrap"
        });
        $("#form").submit(function (event) {
            event.preventDefault();
            $("#btn_added").html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i>");
            $.ajax({
                url: "<?=base_url('admin/course/students/add/' . $id)?>",
                method: "post",
                data: $(this).serialize(),
                success: function (response) {

                    var data = JSON.parse(response);
                    // if (data.result == "1") {
                    $("#btn_added").html("<i class='fa fa-plus'></i><?=translate('add')?>");
                    table.api().ajax.reload();
                    $("#form").reset();
                    // }
                }
            });
        });
        $("#add_mark").submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: "<?=base_url('admin/course/students/set_mark')?>",
                method: "post",
                data: $("#add_mark").serialize(),
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.result == 1) {
                        $("#modal_mark").modal('hide');
                        table.api().ajax.reload();
                    } else {
                        $("#error_mark").html(data.mark);
                    }

                }
            });
        });
    });
    var name_table = "admin/course/students";

</script>