<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-md-10">
                        <h3 style="color: #00A7E5"><?= translate('courses table') ?></h3>

                    </div>
                    <div class="col-md-2 ">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-plus"></i>    <?= translate('options') ?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="<?= base_url('admin/course/new') ?>"><i class="fa fa-plus"></i> <?= translate('new course') ?></a></li>
                                <li><a href="<?= base_url('admin/course/import') ?>"><i
                                            class="fa fa-file-excel-o"></i> <?= translate('import courses') ?> </a></li>
                                <li><a href="<?= base_url('admin/course/import_with_student') ?>"><i
                                            class="fa fa-table"></i> <?= translate('import courses with student') ?> </a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        <hr>
                        <table class="table">
                            <thead>
                            <th><?= translate('#') ?></th>
                            <th><?= translate('Title') ?></th>
                            <th><?= translate('cost') ?></th>
                            <th><?= translate('Count Hours') ?></th>
                            <th><?= translate('country') ?></th>
                            <th><?= translate('city') ?></th>
                            <th><?= translate('location') ?></th>
                            <th><?= translate('State') ?></th>
                            <th><?= translate('Date Added') ?></th>
                            <th><?= translate('option') ?></th>
                            </thead>
                        </table>
                        <br>

                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var table;
    $(document).ready(function () {
        table = $('.table').dataTable({
            "oLanguage": {
                "sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ordering": false,
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=$datatable?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                }, {
                    "data": "title"
                }, {
                    "data": "cost"
                }, {
                    "data": "count_hours"
                }, {
                    "data": "country"
                }, {
                    "data": "city"
                }, {
                    "data": "location"
                }, {
                    "data": "state"
                }, {
                    "data": "date_added"
                }, {
                    "data": "option"
                },

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
    });
</script>