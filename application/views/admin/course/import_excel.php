<div class="col-md-12">
    <div class="table-responsive" style="overflow-x:auto;">

        <table class="table">
            <thead>
            <th><?= translate('title') ?></th>
            <th><?= translate('count hours') ?></th>
            <th><?= translate('cost') ?></th>
            <th><?= translate('more information') ?></th>
            <th><?= translate('country') ?></th>
            <th><?= translate('city') ?></th>
            <th><?= translate('location') ?></th>
            <th><?= translate('maters') ?></th>
            <th><?= translate('Option') ?></th>
            </thead>
            <tbody>

            <?php
            $i = 0;
            foreach ($data as $item) {

                $i++;
                $countries = $this->db->select('c_name as text , c_id as id ')->get('countries')->result_array();
                $country = $this->db->select('c_name as text , c_id as id ')->like('c_name', $item['country'])->get('countries')->result_array();
                if (count($country) > 0) {
                    $country = $country[0]['id'];
                }
                else {
                    $country = null;
                }
                $cities = $this->db->select('c_name as text , c_id as id ')->get('city')->result_array();
                $city = $this->db->select('c_name as text , c_id as id ')->like('c_name', $item['city'])->get('city')->result_array();
                if (count($city) > 0) {
                    $city = $city[0]['id'];
                }
                else {
                    $city = null;
                }
                $maters_array = $this->db->select('m_title as text , m_id as id ')->get('mater')->result_array();
                $boolean_array = array(array('text' => 'yes', 'id' => 1), array('text' => 'no', 'id' => 0));

                ?>


                <tr id="row_<?= $i ?>">

                    <td><?php create_form_group2('Title', 'title', 'title_' . $i, 'title', 'text', $item['title']); ?>                </td>
                    <td><?php create_form_group2('count_hours', 'count_hours', 'count_hours_' . $i, 'count hours', 'number', $item['count_hours']); ?>                </td>
                    <td><?php create_form_group2('cost', 'cost', 'cost_' . $i, 'cost', 'number', $item['cost']); ?>                </td>
                    <td><?php create_form_group2('more_info', 'more_info', 'more_info_' . $i, 'more information', 'text', $item['more_info']); ?>                </td>
                    <td><?php create_form_group2('location', 'location', 'location_' . $i, 'location', 'text', $item['location']); ?>                </td>
                    <td><?php create_select22('country', 'country', 'country_' . $i, $countries, 'text', 'id', 'country', $country); ?>     </td>
                    <td><?php create_select22('city', 'city', 'city_' . $i, $cities, 'text', 'id', 'city', $city); ?>                       </td>
                    <td><?php create_select22('maters', 'maters', 'maters_' . $i, $maters_array, 'text', 'id', 'maters', $item['maters'], true); ?>                       </td>

                    <td>
                        <button type="button" onclick="submit_function(<?= $i ?>)" id="btn_submit_<?= $i ?>"
                                class="btn btn-primary"><i class="fa fa-check"></i></button>
                    </td>

                </tr>


                <script>

                    function submit_function(id) {
        // var city =$("#city_" + id).val();
                        var formData = new FormData();
                        formData.append('title', $("#title_" + id).val());
                        formData.append('hours', $("#count_hours" + id).val());
                        formData.append('location', $("#location_" + id).val());
                        formData.append('cost', $("#cost_" + id).val());
                        formData.append('city', $("#city_" + id).val());
                        formData.append('more', $("#more_info" + id).val());
                        formData.append('maters', $("#maters_" + id).val());
                        $("#btn_submit_" + id).html('<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>').attr('disabled', 'disabled');
                        $.ajax({
                            url: "<?=base_url('admin/course/add')?>",
                            data: formData,
                            method: "post",
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false,
                            success: function (response) {
                                var data = JSON.parse(response);
                                if (data.result == 1) {
                                    $("#btn_submit_" + id).html('<?=translate("Done")?>');
                                    setTimeout(function () {
                                        $("#row_" + id).hide();
                                    }, 3000)
                                } else {
                                    $("#error_title_" + id).html(data.title);
                                    $("#error_hours_" + id).html(data.hours);
                                    $("#error_cost_" + id).html(data.cost);
                                        $("#error_city_" + id).html(data.city);

                                   $("#btn_submit_" + id).html('<i class="fa fa-check"></i>').removeAttr('disabled');

                                }

                            }
                        });
                    }
                </script>
                <?php
            } ?>
            </tbody>
        </table>
    </div>
</div>
