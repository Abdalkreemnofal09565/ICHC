<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3><?= translate('add new Teacher to course') ?>
                <sup>(
                    <?php
                    $title = $this->db->get_where('course', array('c_id' => $id))->row()->c_title;
                    echo $title;
                    ?>)
                </sup>
            </h3>

            <br>
        </div>

        <div class="col-md-12">
            <form id="form">
                <input type="hidden" name="course" value="<?= $id ?>">
                <div class="row bg-grey  ">
                    <div class="col-md-9  ">
                        <div class="form-group">
                            <br>
                            <select class="form-control input-lg" name="teacher" id="">
                                <?php foreach ($teachers as $item) { ?>
                                    <option id="" value="<?= $item['id'] ?>"><?= $item['text'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <br>
                        <button id="btn_added" class="btn btn-primary btn-sm" style="width: 100%"><i
                                    class="fa fa-plus"></i><?= translate('add') ?></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12">

            <hr>
            <table class="table">
                <thead>
                <th><?= translate('#') ?></th>
                <th><?= translate('Name Arabic') ?></th>
                <th><?= translate('Name English') ?></th>
                <th><?= translate('Mobile') ?></th>
                <th><?= translate('Email') ?></th>
                <th><?= translate('telephone') ?></th>
                <th><?= translate('Date Added') ?></th>
                <th><?= translate('option') ?></th>
                </thead>
            </table>
            <br>

            <br>
        </div>

    </div>
</div>

<script>

    $(document).ready(function () {
        var table;
        table = $('.table').dataTable({
            "oLanguage": {
                "sUrl": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Arabic.json"
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ordering": false,
            "searching": true,
            "select": true,
            "ajax": {
                "url": "<?=$datatable?>",
                "type": "POST"
            },

            "columns": [
                {
                    "data": "num"
                }, {
                    "data": "name_ar"
                }, {
                    "data": "name_en"
                }, {
                    "data": "mobile"
                }, {
                    "data": "email"
                }, {
                    "data": "telephone"
                }, {
                    "data": "date_added"
                }, {
                    "data": "option"
                },

            ],
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
        $("select").select2({
            theme: "bootstrap"
        });
        $("#form").submit(function (event) {
            event.preventDefault();
            $("#btn_added").html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i>");
            $.ajax({
                url: "<?=base_url('admin/course/add_teachers/' . $id)?>",
                method: "post",
                data: $(this).serialize(),
                success: function (response) {

                    var data = JSON.parse(response);
                    // if (data.result == "1") {
                    $("#btn_added").html("<i class='fa fa-plus'></i><?=translate('add')?>");
                    table.api().ajax.reload();
                    $("#form").reset();
                    // }
                }
            });
        });
        $(".remove_teachers").on('click', function () {
            alert(this.id);
        });
    });
    var name_table = "admin/course/teachers";

</script>