<div class="container">
    <div class="row">
        <br>
        <br>
        <h3>New Course</h3>
        <hr>

        <div class="col-md-8 col-md-offset-2">
            <form id="form">
                <div class="row">
                    <?php
                    create_form_group("title", "title", 'title', 'title', 'text');
                    create_form_group("count hours", "hours", 'hours', 'count hours', 'number');
                    create_form_group("Cost", "cost", 'cost', 'cost', 'number');
                    $country = $this->db->get('countries')->result_array();
                    create_select2('countries', 'country', 'country', $country, 'c_name', 'c_id', 'countries');
                    $city = array();
                    create_select2('city', 'city', 'city', $city, 'c_name', 'c_id', 'city');
                    create_form_group("Location", "location", 'location', 'location', 'text','',12);

                    $maters = $this->db->get_where('mater', array('state' => 1))->result_array();
                    create_select2('Maters', 'maters[]', 'maters', $maters, 'm_title', 'm_id', 'city', false, true,12);
                    create_form_group_editor("more information", "more", 'more', 'more..');
                    ?>
                    <div class="col-md-12">
                        <button class="btn btn-primary"><?= translate('save') ?></button>
                        <br>
                        <br>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.summernote').summernote();
        $("#form").submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: "<?=base_url('admin/course/add')?>",
                method: "post",
                data: $(this).serialize(),
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.result == 0) {
                        $("#error_title").html(data.title);
                        $("#error_hours").html(data.hours);
                        $("#error_cost").html(data.cost);
                        $("#error_city").html(data.city);
                    }
                }
            });
        });
        $("#country").change(function () {
            $("#city").select2({
                theme: "bootstrap",
                ajax: {
                    url: "<?=base_url('pages/city_list/')?>" + $("#country").val(),
                    dataType: 'json',
                    type: "GET",
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });
        });
    });
</script>