<style>
    .my-card {
        border-left: 4px rgb(41, 182, 246) solid;
        margin-top: 3%;
        box-shadow: -1px 3px 1px rgba(0, 0, 0, .05);
        padding-bottom: 13px;
    }

    .code {
        /*background: #666 none repeat scroll 0 0;*/
        border: 0 none;
        /*-webkit-border-radius: 6px;*/
        /*border-radius: 6px;*/
        /*color: #fff;*/
        display: block;
        /*font-family: consolas, monaco, "andale mono", monospace;*/
        /*font-weight: normal;*/
        /*line-height: 1.8;*/
        /*font-size: medium;*/
        margin: 8px 0;
        padding: 2px;
        /*white-space: pre-wrap;*/
        box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, .05);
    }
</style>
<section id="subheader" data-speed="8" data-type="background" class="padding-top-bottom subheader"
         style="background-position: 50% 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="text-transform: uppercase"><?= translate('All Courses') ?></h1>
                <ul class="breadcrumbs">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <b>/</b>
                    <li class="active">All Courses</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">

        <div  class="col-md-3 col-sm-12">
            <div class="row bg-grey" style="margin-top: 12%;margin-bottom: 12%">
                <div class="col-md-12 col-sm-12 text-center">
                    <br>
                    <h1><?= translate('filter') ?></h1>
                    <hr style="border: 0;
    height: 2px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0),rgb(41, 182, 246), rgba(0, 0, 0, 0));
    margin: 0px 0px 30px;">
                </div>
                <div class="col-md-12 col-sm-12">
                    <label for="country"><?= translate('country') ?></label>
                    <select name="" class="form-control" id="country">
                        <option value=""><?= translate('all countries') ?></option>
                        <?php
                        foreach ($countries as $country) {
                            ?>
                            <option value="<?= $country['c_id'] ?>"><?= $country['c_name'] ?></option>

                            <?php
                        }

                        ?>

                    </select>
                </div>
                <div class="col-md-12 col-sm-12">
                    <label for="country"><?= translate('city') ?></label>
                    <select name="city" class="form-control" id="city"></select>
                </div>
                <div class="col-md-12 col-sm-12">
                    <label for="country"><?= translate('Teachers') ?></label>
                    <select name="teachers" class="form-control" id="teachers">
                        <option value=""><?= translate('all teachers') ?></option>
                        <?php
                        foreach ($teachers as $teacher) {
                            ?>
                            <option value="<?= $teacher['u_id'] ?>"><?= $teacher['u_name_ar'] . ' - ' . $teacher['u_name_en'] ?></option>
                            <?php
                        }
                        ?>

                    </select>
                </div>
                <div class="col-md-12 col-sm-12">
                    <label for="training"><?= translate('Training center') ?></label>
                    <select name="training" class="form-control" id="training">
                        <option value=""><?= translate('all training center') ?></option>
                        <?php
                        foreach ($teachers as $teacher) {
                            ?>
                            <option value="<?= $teacher['u_id'] ?>"><?= $teacher['u_name_ar'] . ' - ' . $teacher['u_name_en'] ?></option>
                            <?php
                        }
                        ?>


                    </select>
                </div>

                <div class="col-md-12 col-sm-12">
                    <br>
                    <hr style="border: 0;
    height: 2px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0),rgb(41, 182, 246), rgba(0, 0, 0, 0));
    margin: 0px 0px 30px;">
                    <button class="btn btn-primary" disabled style="width: 100%"><?= translate('Search') ?> <i
                                class="fa fa-search"></i></button>
                    <br>
                    <br>
                    <br>
                </div>

            </div>


        </div>
        <div class="col-md-8 col-md-offset-1 col-sm-12" id="content-c" style="margin-bottom: 4vh; ">
            <div class="row">
                <div class="col-md-6">
                    <br>
                    <h5><b><?=translate('count all :')?> <?=$count_course?></b></h5>
                </div>
                <div class="col-md-6 ">

                    <div class="text-right">
                        <?= $pages_number ?>
                    </div>
                </div>
            </div>

            <?php foreach ($courses as $course) {
                ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12 my-card">
                        <div class="bs-callout bs-callout-danger" id="callout-glyphicons-dont-mix">
                            <h4 class="text-primary">
                                <span class="text-left"><?= $course['course']['c_title'] ?></span>
                                <span class="pull-right text-muted">
                                    <span class="label label-default"><i
                                                class="fa fa-clock-o"></i> <?= $course['course']['c_count_hours'] ?></span>
                                    <span class="label label-default"><i
                                                class="fa fa-usd"></i> <?= $course['course']['c_cost'] ?></span>
                                    </span>

                            </h4>
                            <p>
                            <h5 class="text-warning"><i
                                        class="fa fa-map-marker"></i> <?= $course['course']['country_name'] . ' - ' . $course['course']['city_name'] . ' - ' . $course['course']['c_location'] ?>
                            </h5>
                            <h5 class="text-muted"><?= $course['course']['c_description'] ?></h5>
                            <div class="panel panel-default  code">
                                <div class="panel-body" style="padding: 8px">
                                    <b> <i class="fa fa-graduation-cap"></i> <?= translate('Teachers:') ?></b>
                                    <?php foreach ($course['teachers'] as $teacher) {
                                        ?>
                                        <a style="color: #fff ; margin-right: 3px"
                                           href="<?= base_url('pages/teacher/' . $teacher['u_id']) ?>">
                                            <h2 class="label label-primary label-larg ">
                                                #<?= $teacher['u_name_ar'] . '_' . $teacher['u_name_en'] ?>
                                            </h2>
                                        </a>

                                        <?php
                                    } ?>
                                </div>
                            </div>
                            <div class="panel panel-default code      ">
                                <div class="panel-body" style="padding: 8px">
                                    <b> <i class="fa fa-home"></i> <?= translate('Training Centers:') ?></b>
                                    <?php foreach ($course['training_centers'] as $training_center) {
                                        ?>
                                        <a style="color: #fff ; margin-right: 3px"
                                           href="<?= base_url('pages/training_center/' . $training_center['u_id']) ?>">
                                            <h2 class="label label-primary label-larg ">
                                                #<?= $training_center['u_name_ar'] . '_' . $training_center['u_name_en'] ?>
                                            </h2>
                                        </a>

                                        <?php
                                    } ?>
                                </div>
                            </div>
                            <div class="panel panel-default code      ">
                                <div class="panel-body" style="padding: 8px">
                                    <b> <i class="fa fa-book"></i> <?= translate('Maters:') ?></b>
                                    <?php foreach ($course['maters'] as $mater) {
                                        ?>
                                        <a style="color: #fff ; margin-right: 3px"
                                           href="<?= base_url('pages/mater/' . $mater['m_id']) ?>">
                                            <h2 class="label label-primary label-larg ">
                                                #<?= $mater['m_title'] . '_' . $mater['m_title'] ?>
                                            </h2>
                                        </a>

                                        <?php
                                    } ?>
                                </div>
                            </div>
                            </p>
                            <div class="row">

                                <div class="col-md-12 text-right">
                                    <a href="<?= base_url('pages/course/' . $course['course']['c_id']) ?>"
                                       class="btn btn-primary" style="border-radius: 0"><?= translate('Show more...') ?></a>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <?
            } ?>

        </div>
    </div>
</div>
<script>
    function load_data(page){
        $.ajax({
            url:"<?php echo base_url(); ?>pages/ajax_pagination/"+page,
            success:function(data)
            {
                $("#content-c").html(data);
            }
        });

    }
    $(document).ready(function () {
        $("select").select2({
            'theme': "bootstrap"
        });
        $("#country").change(function () {
            $("#city").select2({
                theme: "bootstrap",
                ajax: {
                    url: "<?=base_url('pages/city_list/')?>" + $("#country").val(),
                    dataType: 'json',
                    type: "GET",
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });
        });
        $(document).on("click", ".pagination li a", function(event){
            event.preventDefault();
            var page = $(this).data("ci-pagination-page");
            // load_country_data(page);
            // alert(page);
            load_data(page);
        });


    });
</script>