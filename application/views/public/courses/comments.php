<?php
foreach ($comments as $comment) {
    ?>
    <li class="comment even thread-even depth-1" id="comment-5">
        <article class="comment-body">
            <footer class="comment-meta">
                <div class="comment-author vcard">
                    <img class="avatar"
                         src="<?php if($comment['u_profile']=="") echo base_url('user.png'); else echo base_url($comment['u_profile']) ;?>" alt="user image">
                    <b class="fn"><a class="url" href="#">
                            <?=$comment['u_name_ar'].' | ' .$comment['u_name_en']?>
                        </a></b>

                </div><!-- .comment-author -->
                <div class="comment-metadata">
                    <a href="#">
                        <time>
                          <?=$comment['cc_date_added']?>
                        </time>
                    </a>
                </div><!-- .comment-metadata -->
            </footer><!-- .comment-meta -->

            <div class="comment-content">
                <p> <?=$comment['cc_comment']?></p>
            </div><!-- .comment-content -->


        </article><!-- .comment-body -->
    </li>
<?php } ?>