<div id="content" class="no-padding">

    <!-- section begin -->
    <section id="service-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="service-text-intro text-center">
                        <h2><?= $courses['course']['c_title'] ?></h2>
                        <p><?= $courses['course']['c_description'] ?></p>
                    </div>
                    <p class="text-center btn-service-intro">
                        <a href="register.html" class="btn btn-primary ">Get Started</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="section-about">

        <div class="container">

            <div class="row">
                <div class="col-md-9">
                    <div class="post-metadata">
                                        <span class="posted-on">
                                            <i class="fa fa-clock-o"></i> <?= translate('count hours') ?>
                                   :    <?= $courses['course']['c_count_hours'] ?> <sub><?= translate('hours') ?></sub>
                                        </span>
                        <span class="byline">
                                            <i class="fa fa-usd"></i><?= translate('price') ?>:
                                         <?= $courses['course']['c_cost'] ?><sub>
                        <?= translate('$') ?>
                    </sub>
                                        </span>
                        <span class="cat-links">
                                            <i class="fa fa-folder-open"></i><?= translate('count maters') ?>
                                     :       <a
                                    href="#"><?= count($courses['maters']) ?></a> <sub><?= translate('mater') ?></sub>
                                        </span>
                        <span class="comment-link">
                                            <i class="fa fa-comments-o"></i>
                                            <a href="#">29 comments</a>
                                        </span>
                    </div>
                    <aside class="widget widget_text">
                        <h3 class="widget-title"><?= translate('Description') ?></h3>
                        <div class="tiny-border"></div>
                        <div class="textwidget">
                            <p><?= $courses['course']['c_more_info'] ?></p>
                        </div>
                    </aside>
                    <div class="comments-area" id="comments">
                        <h3 class="widget-title"><?= translate('Comments and Rating') ?></h3>
                        <div class="tiny-border"></div>
                        <ol class="comment-list" id="comment-list">


                        </ol>
                        <div class="comment-respond" id="respond">


                            <form class="comment-form" id="commentform">
                                <p id="comment-notes" class="comment-notes text-danger">
                                <?php if (!isset($_SESSION['id']) || (isset($_SESSION['id']) && $_SESSION['user_type'] != 1)) {
                                    ?>

                                        <i
                                                class="fa fa-warning"></i><?= translate('You must be logged ( By Account Student ) in to post a comment.') ?>


                                    <?php
                                }
                                ?>

                                </p>
                                <input type="hidden" value="<?= $id ?>" name="id">


                                <p class="comment-form-comment">
                                    <textarea style="resize: unset ; height: 100px"
                                              rows="1" <?php if (!isset($_SESSION['id']) || (isset($_SESSION['id']) && $_SESSION['user_type'] != 1)) {
                                        echo 'disabled';
                                    } ?>   placeholder="Message*" name="comment" id="comment"></textarea>
                                </p>

                                <p class="form-submit">
                                    <input type="submit" <?php if (!isset($_SESSION['id']) || (isset($_SESSION['id']) && $_SESSION['user_type'] != 1)) {
                                        echo 'disabled';
                                    } ?> value="Post Comment" class="submit" id="submit" name="submit">
                                </p>
                            </form>
                        </div><!-- #respond -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="main-sidebar">
                        <aside class="widget widget_text">
                            <h3 class="widget-title"><?= translate('About') ?></h3>
                            <div class="tiny-border"></div>
                            <div class="textwidget">
                                <p>
                                    <?= $courses['course']['c_description'] ?>

                                </p>
                            </div>
                        </aside>
                        <aside class="widget widget_text">
                            <h3 class="widget-title"><i class="fa fa-map-marker"></i> <?= translate('Location') ?></h3>
                            <div class="tiny-border"></div>
                            <div class="textwidget">
                                <p>
                                    <b>   <?= translate('country') ?> : </b><?= $courses['course']['country_name'] ?>
                                    <br>
                                    <b>   <?= translate('City') ?> :</b> <?= $courses['course']['city_name'] ?><br>
                                    <b>   <?= translate('Details') ?> :</b> <?= $courses['course']['c_location'] ?><br>

                                </p>
                            </div>
                        </aside>

                        <aside class="widget widget_categories">
                            <h3 class="widget-title"><?= translate('Teachers') ?></h3>
                            <div class="tiny-border"></div>
                            <ul>
                                <?php foreach ($courses['teachers'] as $teacher) { ?>
                                    <li class="cat-item"><a href="#">
                                            <?php if ($teacher['tـcertified'] == 1) {
                                                ?>
                                                <i class="text-success fa fa-check-circle-o"></i>
                                                <?php
                                            } ?>

                                            <?= $teacher['u_name_ar'] ?>
                                            (<?= $teacher['u_name_en'] ?>) </a></li>
                                <?php } ?>
                            </ul>
                        </aside>

                        <aside class="widget widget_tag_cloud">
                            <h3 class="widget-title"><?= translate('Maters') ?></h3>
                            <div class="tiny-border"></div>
                            <div class="tagcloud">
                                <?php foreach ($courses['maters'] as $mater) { ?>
                                    <a href="#"><?= $mater['m_title'] ?></a>
                                <?php } ?>
                            </div>
                        </aside>
                        <aside class="widget widget_archive">
                            <h3 class="widget-title"><?= translate('Training center') ?></h3>
                            <div class="tiny-border"></div>
                            <ul>
                                <?php foreach ($courses['training_centers'] as $training_center) { ?>
                                    <li><a href="#">
                                            <?php if ($training_center['tc_certified'] == 1) {
                                                ?>
                                                <i class="text-success fa fa-check-circle-o"></i>
                                                <?php
                                            } ?>
                                            <?= $training_center['u_name_ar'] ?>(<?= $training_center['u_name_en'] ?>)
                                    </li>
                                <?php } ?>
                            </ul>
                        </aside>

                    </div>
                </div>
            </div>
        </div>
    </section>


</div>
<script>
    // $("#comment-list").html('<div class="row text-center"><i class="fa fa-spinner fa-spin fa-5x fa-fw"></i></div>');
    load_comments();

    function load_comments(insert = false) {
        var id = "<?=$id?>";
        $("#comment-list").html('<div class="row text-center"><i class="fa fa-spinner fa-spin fa-5x fa-fw"></i></div>');
        $.ajax({
            url: "<?=base_url('pages/get_comments')?>",
            data: {id: "<?=$id?>", insert: insert},
            method: "post",
            success: function (result) {
                $("#comment-list").html(result);
            }
        });
    }

    $(document).ready(function () {
        $("#commentform").submit(function (event) {
            event.preventDefault();

            $.ajax({
                url: "<?=base_url('pages/set_comment')?>",
                data: $("#commentform").serialize(),
                method: "post",
                success: function (result) {

                    var data = JSON.parse(result);
                    $("#comment-notes").html("");
                    if (data.result == 1)
                        load_comments(true);
                    else
                        $("#comment-notes").html(data.error);
                }
            });
        });
    });
</script>