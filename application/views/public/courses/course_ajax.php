    <div class="row">
        <div class="col-md-6">
            <br>
            <h5><b><?=translate('count all :')?> <?=$count_course?></b></h5>
        </div>
        <div class="col-md-6 ">

            <div class="text-right">
                <?= $pages_number ?>
            </div>
        </div>
    </div>

    <?php foreach ($courses as $course) {
        ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 my-card">
                <div class="bs-callout bs-callout-danger" id="callout-glyphicons-dont-mix">
                    <h4 class="text-primary">
                        <span class="text-left"><?= $course['course']['c_title'] ?></span>
                        <span class="pull-right text-muted">
                                    <span class="label label-default"><i
                                            class="fa fa-clock-o"></i> <?= $course['course']['c_count_hours'] ?></span>
                                    <span class="label label-default"><i
                                            class="fa fa-usd"></i> <?= $course['course']['c_cost'] ?></span>
                                    </span>

                    </h4>
                    <p>
                    <h5 class="text-warning"><i
                            class="fa fa-map-marker"></i> <?= $course['course']['country_name'] . ' - ' . $course['course']['city_name'] . ' - ' . $course['course']['c_location'] ?>
                    </h5>
                    <h5 class="text-muted"><?= $course['course']['c_description'] ?></h5>
                    <div class="panel panel-default  code">
                        <div class="panel-body" style="padding: 8px">
                            <b> <i class="fa fa-graduation-cap"></i> <?= translate('Teachers:') ?></b>
                            <?php foreach ($course['teachers'] as $teacher) {
                                ?>
                                <a style="color: #fff ; margin-right: 3px"
                                   href="<?= base_url('pages/teacher/' . $teacher['u_id']) ?>">
                                    <h2 class="label label-primary label-larg ">
                                        #<?= $teacher['u_name_ar'] . '_' . $teacher['u_name_en'] ?>
                                    </h2>
                                </a>

                                <?php
                            } ?>
                        </div>
                    </div>
                    <div class="panel panel-default code      ">
                        <div class="panel-body" style="padding: 8px">
                            <b> <i class="fa fa-home"></i> <?= translate('Training Centers:') ?></b>
                            <?php foreach ($course['training_centers'] as $training_center) {
                                ?>
                                <a style="color: #fff ; margin-right: 3px"
                                   href="<?= base_url('pages/training_center/' . $training_center['u_id']) ?>">
                                    <h2 class="label label-primary label-larg ">
                                        #<?= $training_center['u_name_ar'] . '_' . $training_center['u_name_en'] ?>
                                    </h2>
                                </a>

                                <?php
                            } ?>
                        </div>
                    </div>
                    <div class="panel panel-default code      ">
                        <div class="panel-body" style="padding: 8px">
                            <b> <i class="fa fa-book"></i> <?= translate('Maters:') ?></b>
                            <?php foreach ($course['maters'] as $mater) {
                                ?>
                                <a style="color: #fff ; margin-right: 3px"
                                   href="<?= base_url('pages/mater/' . $mater['m_id']) ?>">
                                    <h2 class="label label-primary label-larg ">
                                        #<?= $mater['m_title'] . '_' . $mater['m_title'] ?>
                                    </h2>
                                </a>

                                <?php
                            } ?>
                        </div>
                    </div>
                    </p>
                    <div class="row">

                        <div class="col-md-12 text-right">
                            <a href="<?= base_url('pages/course/' . $course['course']['c_id']) ?>"
                               class="btn btn-primary" style="border-radius: 0"><?= translate('Show more...') ?></a>

                        </div>
                    </div>
                </div>


            </div>
        </div>

        <?
    } ?>


