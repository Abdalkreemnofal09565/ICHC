<div class="container">
    <div class="row">
        <div class="col-md-12"><h1><?= translate('All Students') ?></h1>
            <hr>
        </div>
        <?php
        foreach ($students as $student) {
            ?>
            <div class="col-sm-6 col-md-4 col-md-offset-1" style="margin-top: 35px">
                <div class="row">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-3 clearfix">
                            <div class="thumbnail" style="padding: 0px ; margin: 0px">
                                <img src="<?= base_url('upload/user/user_'.$student['user'].'.png') ?>"  style="width: 100vw" onerror="this.onerror=null; this.src='<?=base_url('student.png')?>'">
                            </div>

                        </div>
                        <div style="border-left: 3px solid #47b9f8;border-bottom: 3px solid #47b9f8;" class="col-md-8 col-sm-6 col-xs-9">
                            <div class="text-primary"><?php
                                if($_SESSION['language']=="arabic") echo $student['u_name_ar'];
                                else $student['u_name_en'];
                                ?></div>
                            <h5><i class="fa fa-map-marker text-primary"></i><?='   '.$student['country_name'].' - '.$student['city_name']?></h5>
                            <?php
                            $phone = "";
                            if($student['u_mobile']!= null and $student['u_mobile']!= ""){
                                $phone =$student['u_mobile'];
                            }elseif($student['u_telephone']!= null and $student['u_telephone']!= ""){
                                $phone =$student['u_telephone'];
                            }
                            if ($phone !="")
                            {   ?>
                            <h5><i class="fa fa-phone text-primary"></i> <?php echo $phone ?></h5>
                            <?php } ?>
                            <a style="width: 100%;direction: rtl"  href="<?=base_url('pages/student/'.$student['u_id'])?>" class="text-primary  text-left">read more .. </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>

</div>