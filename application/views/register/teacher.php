<form id="REGISTER_STUDENT">
    <div class="container">
        <div class="row">
            <div style=";margin-top: 5vh;margin-bottom: 5vh; " class="bg-grey col-md-offset-2 col-md-8">
                <div class="row">
                    <div class="col-md-12 text-center  " style="background: #47b9f8">
                        <br>
                        <h1 style="color: #fff" class="h1"><?= translate('Register teacher') ?></h1>
                        <p>
                            <a class="text-white text-small"
                               href="#"><?= translate('Register with a student account?') . ' ' ?></a>
                            <a class="text-white text-small"
                               href="#"><?= translate('Register with the Institute account?') ?></a>
                        </p>
                        <hr style="border: 0;
    height: 2px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0),rgb(207, 51, 83), rgba(0, 0, 0, 0));
    margin: 15px 0px 15px;">
                        <p><a class="text-white" href="#">Do you have an account? Sign in</a></p>
                        <br>
                    </div>

                </div>
                <div class="row">
                    <br>
                    <!--                    <div class="col-md-3">-->
                    <!--                        <div class="col-md-12">-->
                    <!--                            <br>-->
                    <!--                            <div class="col-md-12 ">-->
                    <!--                                <img class="img-circle img-thumbnail" id="profilePicture" style="width: 100%"-->
                    <!--                                     src="--><?//= base_url('user.png') ?><!--" alt="">-->
                    <!---->
                    <!--                            </div>-->
                    <!--                            <div class="col-md-12" onclick="getFile()">-->
                    <!--                                <button class="btn btn-primary btn-sm" id="yourBtn" w type="button"-->
                    <!--                                        style="padding:unset;width: 100%; border-radius: 0px ;padding-top: 3px ;padding-bottom: 3px;margin-top: 5px">--><?//= translate('change photo') ?>
                    <!--                                </button>-->
                    <!--                            </div>-->
                    <!--                            <div style='height: 0px;width: 0px; overflow:hidden;'>-->
                    <!--                                <input id="upfile" type="file"-->
                    <!--                                       value="upload"-->
                    <!--                                /></div>-->
                    <!---->
                    <!---->
                    <!--                        </div>-->
                    <!---->
                    <!--                    </div>-->
                    <div class="col-md-10 col-md-offset-2">

                        <div class="form-group col-md-9  col-sm-12">
                            <label for="email"><?= translate('email') ?><b class="text-danger">*</b></label>
                            <input onautocomplete="false" autocomplete="off" name="email" type="email" id="email"
                                   class="form-control" style="border-radius: 0">
                            <b id="error_email" class="text-danger"></b>
                        </div>
                        <div class="form-group col-md-9  col-sm-12">
                            <label for="password"><?= translate('password') ?> <b class="text-danger">*</b></label>
                            <input type="password" name="password" id="password" class="form-control"
                                   style="border-radius: 0">
                            <b id="error_password" class="text-danger"></b>
                        </div>
                        <div class="form-group col-md-9  col-sm-12">
                            <label for="arabicname"><?= translate('arabic name') ?><b class="text-danger">*</b></label>
                            <input type="text" name="arabicname" id="arabicname" class="form-control"
                                   style="border-radius: 0">
                            <b id="error_arabicname" class="text-danger"></b>
                        </div>
                        <div class="form-group col-md-9  col-sm-12">
                            <label for="englishname"><?= translate('english name') ?><b
                                    class="text-danger">*</b></label>
                            <input type="text" name="englishname" id="englishname" class="form-control"
                                   style="border-radius: 0">
                            <b id="error_englishname" class="text-danger"></b>
                        </div>
                        <div class="form-group col-md-9  col-sm-12">
                            <label for="mobile"><?= translate('mobile') ?> <b class="text-danger">*</b></label>
                            <input type="text" name="mobile" id="mobile" class="form-control" style="border-radius: 0">
                            <b id="error_mobile" class="text-danger"></b>
                        </div>
                        <div class="form-group col-md-9  col-sm-12">
                            <label for="telephone"><?= translate('telephone') ?></label>
                            <input type="text" name="telephone" id="telephone" class="form-control"
                                   style="border-radius: 0">
                            <b id="error_telephonel" class="text-danger"></b>
                        </div>

                        <div class="form-group col-md-9  col-sm-12">
                            <label for="sex"><?= translate('gender') ?><b class="text-danger">*</b></label>
                            <select name="sex" class="form-control  select2-single" style="border-radius: 0"
                                    id="sex"></select>
                            <b id="error_sex" class="text-danger"></b>
                        </div>
                        <div class="form-group col-md-9  col-sm-12">
                            <label for="country"><?= translate('country') ?><b class="text-danger">*</b></label>
                            <select name="country" class="form-control  select2-single" style="border-radius: 0"
                                    id="country"></select>
                            <b id="error_country" class="text-danger"></b>
                        </div>
                        <div class="form-group col-md-9  col-sm-12">
                            <label for="city"><?= translate('city') ?><b class="text-danger">*</b></label>
                            <select name="city" class="form-control  select2-single" style="border-radius: 0"
                                    id="city"></select>
                            <b id="error_city" class="text-danger"></b>
                        </div>
                        <!--                        <div class="form-group col-md-9  col-sm-12">-->
                        <!--                            <label for="facebook">-->
                        <? //= translate('facebook url') ?><!--</label>-->
                        <!--                            <input name="facebook" type="text" placeholder="https://www.facebook.com/usrename"-->
                        <!--                                   id="facebook"-->
                        <!--                                   class="form-control" style="border-radius: 0">-->
                        <!--                            <b id="error_facebook" class="text-danger"></b>-->
                        <!--                        </div>-->
                        <div class="form-group col-md-9  col-sm-12">
                            <button class="btn-primary btn" id="submit_btn" style="width: 100%;border-radius: 0">
                                SAVE
                            </button>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</form>
<script>
    $(document).ready(function () {
        $("#sex").select2({
            theme: "bootstrap",
            ajax: {
                url: "<?=base_url('pages/sex_list')?>",
                dataType: 'json',
                type: "GET",
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data
                    };
                }
            }
        });

        $("#country").select2({
            theme: "bootstrap",
            ajax: {
                url: "<?=base_url('pages/country_list')?>",
                dataType: 'json',
                type: "GET",
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data
                    };
                }
            }
        });
        $("#country").change(function () {
            $("#city").select2({
                theme: "bootstrap",
                ajax: {
                    url: "<?=base_url('pages/city_list/')?>" + $("#country").val(),
                    dataType: 'json',
                    type: "GET",
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: data
                        };
                    }
                }
            });
        });
    });

    function getFile() {
        document.getElementById("upfile").click();
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profilePicture').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#upfile").change(function () {
        readURL(this);
    });
    $("#REGISTER_STUDENT").on('submit', function (event) {
        event.preventDefault();
        $("#submit_btn").html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i><?=translate("please wait ..")?>').attr('disabled', 'disabled');
        $.ajax({
            url: "<?=base_url('Users/register/teacher')?>",
            data: $(this).serialize(),
            method: "post",
            success: function (response) {
                var data = JSON.parse(response);
                if (data.result == 1) {
                    //success
                    $("#submit_btn").html('<i class="fa fa-check"></i> <?=translate("success to added")?>');
                    $("#message_error").html('<span><?=translate("create_new_user")?></span>');
                    $('#exampleModal').modal('show');
                    setTimeout(function () {
                        location.href = "<?=base_url('pages/login?verify=1')?>";
                    },3000);

                } else {
                    $("#error_email").html(data.email);
                    $("#error_password").html(data.password);
                    $("#error_arabicname").html(data.arabicname);
                    $("#error_englishname").html(data.englishname);
                    $("#error_mobile").html(data.mobile);
                    $("#error_telephonel").html(data.telephone);
                    $("#error_sex").html(data.sex);
                    $("#error_country").html(data.country);
                    $("#error_city").html(data.city);
                    $("#error_facebook").html(data.facebook);
                }
            }
        });
    });
</script>