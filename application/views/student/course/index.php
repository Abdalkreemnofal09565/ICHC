<section id="subheader" data-speed="8" data-type="background" class="padding-top-bottom subheader"
         style="background-position: 50% 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="text-transform: uppercase"><?= translate('My Courses') ?></h1>

            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br>
            <br>
            <table class="table table-bordered">
                <thead class="bg-dark text-center" style="color: #fff; ">
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center">#</th>
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center"><?= translate('Title') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center"><?= translate('Cost') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px"
                    class="text-center"><?= translate('Count hours') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center"><?= translate('Maters') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center"><?= translate('Country') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center"><?= translate('City') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center"><?= translate('Location') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px" class="text-center"><?= translate('Mark') ?></th>
                <th style="padding-top: 15px;padding-bottom: 15px"
                    class="text-center"><?= translate('Date of registration') ?></th>
                </thead>
                <tbody>
                <?php
                $i = 1;
                foreach ($courses as $cours) {
                    $maters = json_decode($cours['c_maters']);
                    $maters = $this->db->where_in('m_id', $maters)->get('mater')->result_array();
                    $mm = "";
                    foreach ($maters as $mater) {
                        $mm .= '   <span style="margin-right: 10px" class="label label-default">#' . $mater['m_title'] . ' </span>   ';
                    }
                    ?>
                    <tr class="text-center" >
                        <td><?= $i++; ?></td>
                        <td><?= $cours['c_title'] ?></td>
                        <td><?= $cours['c_cost'] ?> <i class="fa fa-usd"></i></td>
                        <td><?= $cours['c_count_hours'] ?> <i class="fa fa-clock-o"></i></td>
                        <td><?= $mm ?></td>
                        <td><i class="fa fa-map-marker"></i> <?= $cours['country'] ?></td>
                        <td><i class="fa fa-map-marker"></i> <?= $cours['city'] ?></td>
                        <td><i class="fa fa-map-marker"></i> <?= $cours['c_location'] ?></td>
                        <td <?php if ($cours['cs_examـmark']!="" &&  $cours['cs_examـmark'] < 50) echo 'style="background-color: #d31e00 ; color:#fff"'; ?> ><b><u><?= $cours['cs_examـmark'] ?></u></b></td>
                        <td><?= $cours['cs_date_added'] ?></td>

                    </tr>

                    <?php
                }

                ?>
                </tbody>
                <tfoot>
                <tr class="">
                    <td class="text-center" colspan="10" style="background-color: orange; color: #fff;cursor: pointer">
                        <b>
                            <i class="fa fa-plus"></i>
                            <?= translate('To register for new courses') ?>
                        </b>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>