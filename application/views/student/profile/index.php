
<style>
    #label {
        width: 100%;
        height: 300px;
        /*background: red;*/
        display: table;
        color: #fff;
        border: 3px #47b9f8 dashed;
        text-align: center;

    }

    input[type="file"] {
        display: none;
    }

    #change_image {
        visibility: hidden;
        width: 100%;
        border-radius: 0px;
        position: absolute;
        bottom: 0;
        margin: 0;
        left: 0;

    }

    #image_profile {
        height: 200px;
        background-image: url(<?php if($user['u_profile']!="")echo base_url($user['u_profile']);else echo base_url('user-1.png') ?>);
        background-size: cover;

    }

    #image_profile:hover > #change_image {
        visibility: visible;
        animation: mymove 0.4s;

    }

    #image_profile:hover {
        opacity: 0.9;

    }

    @keyframes mymove {
        from {
            /*bottom:-10%;*/
            opacity: 0.2;
        }
        to {
            /*bottom: 0%;*/
            opacity: 1.0;
        }
    }

    .list-group-item {
        border-radius: 0;
        margin-bottom: 3px;
        border: none;
    }

    .item_menu:hover {
        background-color: #e0e0e0;
    }

</style>
<div class="modal fade" id="update_description" tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form id="save_about">
                    <div class="row">
                        <?php
                        create_form_group_editor('Description', 'description', 'description_1', 'Description..', $student['s_additional_information']); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" id="about_save_btn" style="width: 100%"
                                    class="btn btn-primary"><?= translate('Save') ?></button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" data-dismiss="modal" aria-label="Close" style="width: 100%"
                                    class="btn  btn-danger"><?= translate('Close') ?></button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog " style="width: 350px">
        <div class="modal-content" style="border-radius: 0px">

            <div class="modal-body" style="margin: 0px;padding: 0">
                <div class="row" style="margin: 0px;padding: 0">
                    <div class="col-md-12 text-center" style="margin: 0px;padding: 0">
                        <div id="image_demo" class="hide" style="width:350px; margin-top:30px"></div>
                        <label id="label">
                            <br>
                            <br>
                            <br>
                            <i class="fa fa-5x fa-download" style="color: #47b9f8 ; margin-top: 30px"></i>
                            <h4 style="color: #47b9f8"><?= translate('Please click to select the image') ?></h1>
                                <input type="file" name="upload_image" id="upload_image"/>
                        </label>
                        <br/>
                        <div id="uploaded_image"></div>
                    </div>
                    <div class="col-md-12 text-center" style=" padding: 0 ;margin: 0;border-radius: 0px">

                        <button class="btn btn-primary crop_image"
                                style="width: 100%;border-radius: 0px"><?= translate('change image') ?></button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="container-fluid">
    <br>
    <div class="container">
        <div class="row">

            <div class="col-md-2 col-sm-12">
                <br>
                <div class="row">


                    <div class="col-md-12 col-sm-12" id="image_profile"
                         style="">
                        <button class="btn btn-primary " id="change_image" data-toggle="modal"
                                data-target="#uploadimageModal"
                                style=""> <?= translate('Change Image') ?></button>
                    </div>
                </div>
                <hr>
                <br>
                <div class="row">

                    <ul class="list-group nav nav-pills nav-stacked " style="margin-bottom: 50px">
                        <li role="presentation" class="list-group-item  item_menu" id="item_menu1"
                            style="border-radius: 0px;cursor: pointer" role="tab" href="#about" data-toggle="tab"
                        ><b> <i class="fa fa-plus"></i> </b><?= translate('Description') ?>


                        </li>
                        <li role="presentation" class="list-group-item   item_menu" id="item_menu2" role="tab"
                            href="#information" data-toggle="tab"
                            style="border-radius: 0px;cursor: pointer"><b><i class="fa fa-plus"></i>
                            </b><?= translate('Information') ?></li>
                        <li role="presentation" class="list-group-item  item_menu" id="item_menu3" role="tab"
                            href="#connection" data-toggle="tab"
                            style="border-radius: 0px;cursor: pointer"><b><i
                                        class="fa fa-plus"></i></b> <?= translate('Connection') ?></li>
                        <li role="presentation" class="list-group-item   item_menu" id="item_menu4" role="tab"
                            href="#security" data-toggle="tab"
                            style="border-radius: 0px;cursor: pointer"><b><i
                                        class="fa fa-plus"></i></b> <?= translate('Security') ?></li>

                    </ul>

                </div>


            </div>
            <div class="col-md-9 col-sm-12">
                <div class="post-content">

                    <div class="post-title">
                        <h1>
                            <?php
                            if ($_SESSION['language'] == "arabic")
                                echo @$user['u_name_ar'];
                            else echo @$user['u_name_en'];
                            ?>
                            <sub>
                                (
                                <?php if ($_SESSION['language'] == "arabic")
                                    echo $user['u_name_en'];
                                else echo $user['u_name_ar'];
                                ?>
                                )
                            </sub>
                        </h1>
                    </div>
                    <div class="post-metadata ">
                                        <span class="posted-on">
                                            <i class="fa fa-clock-o"></i>
                                            <?= $user['u_date_register'] ?>
                                        </span>
                        <span class="byline">
                                            <i class="fa fa-mobile"></i>
                                    <a href="tel:<?= $user['u_mobile'] ?>">      <?= $user['u_mobile'] ?></a>
                                        </span>
                        <span class="cat-links">
                                            <i class="fa fa-phone"></i>
                                            <a href="tel:<?= $user['u_telephone'] ?>"><?= $user['u_telephone'] ?></a>
                                        </span>

                    </div>
                    <hr>
                    <div class="post-entry ">
                        <div class="row">
                            <div class="col-md-3">

                            </div>
                        </div>


                        <div>

                            <div class="tab-content">

                                <div role="tabpanel" class="tab-pane    active  " id="about" style="">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h3><?= translate('Description') ?></h3>
                                                </div>
                                                <div class="col-md-6 text-right ">
                                                    <button class=" btn btn-primary " data-toggle="modal"
                                                            style="margin: 0"
                                                            data-target="#update_description"><?= translate('Edit Description') ?></button>
                                                </div>

                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="">
                                                <?= $student['s_additional_information'] ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="information">

                                    <form id="profile">

                                        <div class="row">
                                            <?php
                                            create_form_group3('Email', 'email', 'email', 'Email (Such as : example@mail.com)', 'text', $user['u_email'], '12');
                                            create_form_group3('Name Arabic', 'name_ar', 'name_ar', 'Name Arabic', 'text', $user['u_name_ar'], '12');
                                            create_form_group3('Name english', 'name_en', 'name_en', 'Name english', 'text', $user['u_name_en'], '12');
                                            create_form_group3('Birthday', 'birthday', 'birthday', 'birthday', 'date', $student['s_birthdate'], '12');
                                            create_form_group3('Facebook', 'facebook', 'facebook', 'facebook', 'text', $student['s_facebook'], '12');
                                            create_form_group3('Mobile', 'mobile', 'mobile', 'mobile', 'text', $user['u_mobile'], '12   ');
                                            create_form_group3('Telephone', 'telephone', 'telephone', 'telephone', 'text', $user['u_telephone'], '12');

                                            $gender = $this->db->get('gender')->result_array();
                                            create_select23('gender', 'gender', 'gender', $gender, 'g_title', 'g_id', 'gender', $user['gender'], false, 12);
                                            $country = $this->db->get('countries')->result_array();
                                            create_select23('countries', 'country', 'country', $country, 'c_name', 'c_id', 'countries', $user['country'], false, 12);
                                            $city = $this->db->select('c_name , c_id ')->get('city')->result_array();
                                            create_select23('city', 'city', 'city', $city, 'c_name', 'c_id', 'city', $user['city'], false, 12);
                                            ?>
                                            <div class="col-md-8 col-md-offset-3 text-center">
                                                <button class="btn btn-primary" id="profile_save"
                                                        style="width:100%"><?= translate('Save') ?></button>
                                                <br>
                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div role="tabpanel" class="tab-pane " id="connection">
                                    <form id="connection_form">

                                        <?php

                                        foreach ($social as $item) {
                                            create_form_group3($item['social_name'], "s_" . $item['social_id'], $item['social_id'], $item['social_name'], 'text', $item['value'], '12');
                                        }
                                        ?>
                                        <div class="col-md-8 col-md-offset-3">
                                            <button style="width: 100%;" type="submit" id="btn_save_connection"
                                                    class="btn btn-primary"><?= translate('Save') ?></button>
                                            <br>
                                            <br>
                                        </div>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane " id="security">
                                    <form id="Security_form">
                                        <?php
                                        create_form_group3('Old password', 'old_password', 'old_password', 'Old password', 'password', null, '12');
                                        create_form_group3('New password', 'new_password', 'new_password', 'New password', 'password', null, '12');
                                        create_form_group3('Confirm password', 'confirm_password', 'confirm_password', 'Confirm password', 'password', null, '12');

                                        ?>

                                        <div class="col-md-8 col-md-offset-3">
                                            <button style="width: 100%;" type="submit" id="btn_save_security"
                                                    class="btn btn-primary"><?= translate('Save') ?></button>
                                            <br>
                                            <br>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>



<script>
    $("#save_about").submit(function (event) {
        event.preventDefault();
        $("#about_save_btn").html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i><?=translate('waiting ... ')?>").attr('disabled', 'disabled');
        $.ajax({
            url: "<?=base_url('student/change_description')?>",
            method: "post",
            data: {description: $("#description_1").val()},
            success: function (response) {
                var data = JSON.parse(response);
                if (data.result == 1) {
                    $("#about_save_btn").html("<i class='fa fa-check'></i><?=translate('Done')?>");
                    setTimeout(function () {
                        $("#about_save_btn").html('<?=translate("Save")?>').removeAttr('disabled');
                    }, 2500);
                } else {
                    $("#about_save_btn").html(data.error);
                    setTimeout(function () {
                        $("#about_save_btn").html('<?=translate("Save")?>').removeAttr('disabled');
                    }, 3500);
                }
            }
        });


    });
    $("#profile").submit(function (event) {
        event.preventDefault();
        $("#about_save_btn").html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i><?=translate('waiting ... ')?>").attr('disabled', 'disabled');
        $.ajax({
            url: "<?=base_url('student/update_profile')?>",
            method: "post",
            data: $("#profile").serialize(),
            success: function (response) {
                var data = JSON.parse(response);
                if (data.result == 1) {
                    $("#profile_save").html("<i class='fa fa-check'></i><?=translate('Done')?>");
                    setTimeout(function () {
                        $("#profile_save").html('<?=translate("Save")?>').removeAttr('disabled');
                    }, 2500);
                } else {
                    $("#profile_save").html(data.error);
                    $("#error_name_ar").html(data.name_ar);
                    $("#error_name_en").html(data.name_en);
                    $("#error_email").html(data.email);
                    $("#error_mobile").html(data.mobile);
                    setTimeout(function () {
                            $("#profile_save").html('<?=translate("Save")?>').removeAttr('disabled');
                        }
                        ,
                        3500
                    )
                    ;
                }
            }
        });


    });
    $("#connection_form").submit(function (event) {
        event.preventDefault();
        $("#about_save_btn").html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i><?=translate('waiting ... ')?>").attr('disabled', 'disabled');
        $.ajax({
            url: "<?=base_url('student/update_connection')?>",
            method: "post",
            data: $("#connection_form").serialize(),
            success: function (response) {
                var data = JSON.parse(response);
                if (data.result == 1) {
                    $("#btn_save_connection").html("<i class='fa fa-check'></i><?=translate('Done')?>");
                    setTimeout(function () {
                        $("#btn_save_connection").html('<?=translate("Save")?>').removeAttr('disabled');
                    }, 2500);
                } else {
                    $("#btn_save_connection").html(data.error);

                    setTimeout(function () {
                            $("#btn_save_connection").html('<?=translate("Save")?>').removeAttr('disabled');
                        }
                        ,
                        3500
                    )
                    ;
                }
            }
        });


    });
    $("#Security_form").submit(function (event) {
        event.preventDefault();
        $("#btn_save_security").html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i><?=translate('waiting ... ')?>").attr('disabled', 'disabled');
        $.ajax({
            url: "<?=base_url('users/update_password')?>",
            method: "post",
            data: $("#Security_form").serialize(),
            success: function (response) {
                var data = JSON.parse(response);
                $("#btn_save_security").html(data.error);
                $("#error_old_password").html(data.old_password);
                $("#error_new_password").html(data.new_password);
                $("#error_confirm_password").html(data.confirm_password);

                if (data.result == 1) {
                    $("#btn_save_security").html("<i class='fa fa-check'></i><?=translate('Done')?>");
                    setTimeout(function () {
                        $("#btn_save_security").html('<?=translate("Save")?>').removeAttr('disabled');
                    }, 2500);
                } else {

                    setTimeout(function () {
                            $("#btn_save_security").html('<?=translate("Save")?>').removeAttr('disabled');
                        }
                        ,
                        3500
                    )
                    ;
                }
            }
        });


    });
    $('.item_menu').click(function () {
        $('.item_menu').removeClass('active');
        $(this).addClass('active');
    });
    $("#country").change(function () {
        $("#city").select2({
            theme: "bootstrap",
            ajax: {
                url: "<?=base_url('pages/city_list/')?>" + $("#country").val(),
                dataType: 'json',
                type: "GET",
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data
                    };
                }
            }
        });
    });

    $(document).ready(function () {

        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'square' //circle
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#upload_image').on('change', function () {
            $("#image_demo").removeClass('hide').addClass('show');
            $("#upload_image").removeClass('show').addClass('hide');
            $("#label").removeClass('show').addClass('hide');
            var reader = new FileReader();
            reader.onload = function (event) {
                $image_crop.croppie('bind', {
                    url: event.target.result
                }).then(function () {
                    // console.log('jQuery bind complete');
                });
            }
            reader.readAsDataURL(this.files[0]);
            $('#uploadimageModal').modal('show');
        });

        $('.crop_image').click(function (event) {
            $image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (response) {
                $.ajax({
                    url: "<?=base_url('student/change_my_image')?>",
                    type: "POST",
                    data: {"image": response},
                    success: function (data) {
                        $('#uploadimageModal').modal('hide');
                        $('#uploaded_image').html(data);
                        //location.href = "<?//=base_url('student')?>//";

                    }
                });
            })
        });

    });
</script>
