<!-- header begin -->
<header class="site-header-1 site-header">
    <!-- Main bar start -->
    <div id="sticked-menu" class="main-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <!-- logo begin -->
                    <div id="logo" class="pull-left">
                        <a href="index.html">
                            <img src="images/logo.png" alt="" class="logo">
                        </a>
                    </div>
                    <!-- logo close -->

                    <!-- btn-mobile menu begin -->
                    <a id="show-mobile-menu" class="btn-mobile-menu hidden-lg hidden-md"><i class="fa fa-bars"></i></a>
                    <!-- btn-mobile menu close -->

                    <!-- mobile menu begin -->
                    <nav id="mobile-menu" class="site-mobile-menu hidden-lg hidden-md">
                        <ul></ul>
                    </nav>
                    <!-- mobile menu close -->

                    <!-- desktop menu begin -->
                    <nav id="desktop-menu" class="site-desktop-menu hidden-xs hidden-sm">
                        <ul class="clearfix">
                        <?php 
                    foreach ($nav as $item){
                        ?>
                        <li ><a href=" <?=  $item['data']['h_url']; ?> "> <?=  $item['data']['h_title']; ?> </a>
                        <?php  if(isset($item['children'])){  ?>
                            <ul>
                            <?php
                    foreach ($item['children'] as $item_c){
                        ?>
                             <li><a href="<?=  $item_c['h_url']; ?>"><?=  $item_c['h_title']; ?></a>
                        <?php }?>
                            </ul>
                        <?php }?>
                    <?php
                    }
                  
                    ?>
                            <li class="active"><a href="index.html">Home</a>
                            </li>
                            <li><a href="#">Training</a>
                                <ul>
                                    <li><a href="#">Programs</a>
                                        <ul>
                                            <li><a href="hr.html">Human Resources</a></li>
                                            <li><a href="ed.html">Education Development</a></li>
                                            <li><a href="ldm.html">Leaders and Decision Makers</a></li>
                                            <li><a href="ma.html">Marketing and Advertising</a></li>
                                            <li><a href="epd.html">Enterprise Development</a></li>
                                            <li><a href="fa.html">Financial and Accountance</a></li>
                                            <li><a href="mvw.html">Management of volunteer work</a></li>
                                            <li><a href="mi.html">Management in Institutions</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Courses</a>
                                        <ul>
                                            <li><a href="prm.html">Public Relations and Media</a></li>
                                            <li><a href="qm.html">Quality Management</a></li>
                                            <li><a href="sd.html">Self Development</a></li>
                                            <li><a href="lang.html">Languages</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Deplomas</a>
                                        <ul>
                                            <li><a href="dnlp.html">NLP</a></li>
                                            <li><a href="pnlp.html">NLP Practitioner</a></li>
                                            <li><a href="mnlp.html">NLP Master Practitioner</a></li>
                                            <li><a href="pmp.html">Project Management Professional</a></li>
                                            <li><a href="ss.html">Security and Safety</a></li>
                                            <li><a href="mngo.html">Management of NGOs</a></li>
                                            <li><a href="hrm.html">Hotel and Restaurant Management</a></li>
                                            <li><a href="hm.html">Hospital Management</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Projects</a>
                                        <ul>
                                            <li><a href="egov.html">E-Government</a></li>
                                            <li><a href="sr.html">School Radio</a></li>
                                            <li><a href="qnem.html">Qualification of the new Employee</a></li>
                                            <li><a href="mep.html">Malaysian Experience in Planning</a></li>
                                            <li><a href="ce.html">Career Empowerment</a></li>
                                            <li><a href="sp.html">Simplifying Procedures</a></li>
                                            <li><a href="pe.html">Planning experts</a></li>
                                            <li><a href="tme.html">Training Management Expert</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="comingsoon.html">Coming Soon</a></li>
                                    <li><a href="onsite.html">OnSite</a></li>
                                    <li><a href="online.html">OnLine</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Accreditation</a>
                                <ul>
                                    <li><a href="register.html">Register / Login</a></li>
                                    <li><a href="accrecenter.html">ICHC Accredited Centers</a></li>
                                    <li><a href="accretrainer.html">ICHC Accredited Trainers</a></li>
                                    <li><a href="trainees.html">ICHC Trainees</a></li>
                                    <li><a href="faqs.html">FAQs</a></li>
                                </ul>
                            </li>
                            <li><a href="projects.html">Portfolios</a>
                                <ul>
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="service.html">Services</a></li>
                                    <li><a href="pricing.html">Pricing Tables</a></li>
                                    <li><a href="portfolio.html">ICHC-Portfolio</a></li>
                                </ul>
                            </li>
                            <li><a href="<?=base_url('pages/all_courses')?>"><?=translate('show all courses')?></a>

                            </li>
                            <li><a href="contact.html">Contact ICHC</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- desktop menu close -->
                    <?php
                    if (!isset($_SESSION['user_type'])) {
                        require 'base.php';

                    } else if ($_SESSION['user_type'] == 1) {
                        require 'student.php';
                    } else if ($_SESSION['user_type'] == 2) {
                        require 'teacher.php';
                    } else if ($_SESSION['user_type'] == 4) {
                        require 'admin.php';
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</header>
