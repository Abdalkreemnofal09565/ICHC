<!-- Header Group Button Right begin -->
<div class="header-buttons pull-right hidden-xs hidden-sm">

    <div class="header-contact">
        <ul class="clearfix">
            <li class="phone"><i class="fa fa-phone"></i> <span>+963-9949-333-71</span></li>
            <li class="border-line">|</li>
        </ul>
    </div>

    <!-- Button Modal popup searchbox -->
    <div class="search-button">
        <!-- Trigger the modal with a button -->
        <a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i></a>
    </div>

    <!-- Button Menu OffCanvas right -->
    <div class="navright-button">
        <a href="" id="btn-offcanvas-menu"><i class="fa fa-bars"></i></a>
    </div>

</div>