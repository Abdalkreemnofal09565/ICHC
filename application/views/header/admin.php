<nav class="header-buttons site-desktop-menu pull-right hidden-xs hidden-sm">
    <nav id="desktop-menu" class="site-desktop-menu hidden-xs hidden-sm">
        <ul class="clearfix">


            <li><a href="#"><i class="fa fa-user"></i><?= @$_SESSION['name'][$_SESSION['language']] ?></a>
                <ul>

                    <li><a href="<?= base_url('admin/students') ?>"><?= translate('Students') ?></a></li>
                    <li><a href="<?= base_url('admin/teachers') ?>"><?= translate('Teachers') ?></a></li>
                    <li><a href="<?= base_url('admin/training_center') ?>"><?= translate('Center Training') ?></a></li>
                    <li><a href="<?= base_url('admin/maters') ?>"><?=translate('Mater')?></a></li>
                    <li><a href="<?= base_url('admin/course') ?>"><?=translate('course')?></a></li>
                    <li><a href="<?= base_url('admin/mange_website') ?>">Website Mange</a></li>
                    <li><a href="<?= base_url('admin/') ?>">key Word</a></li>
                    <li><a href="<?= base_url('admin/') ?>">certificates</a></li>
                    <li><a href="<?= base_url('admin/') ?>">blogs</a></li>
                    <li><a href="<?= base_url('admin/') ?>">country</a></li>
                    <li><a href="<?= base_url('admin/') ?>">socile</a></li>

                    <li><a href="<?= base_url('pages/logout') ?>"><?= translate('logout') ?></a></li>
                </ul>
            </li>
            <li>
                <a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i></a>
            </li>
            <li>
                <a href="" id="btn-offcanvas-menu"><i class="fa fa-bars"></i></a>
            </li>
        </ul>
    </nav>

</nav>