<nav class="header-buttons site-desktop-menu pull-right hidden-xs hidden-sm">
    <nav id="desktop-menu" class="site-desktop-menu hidden-xs hidden-sm">
        <ul class="clearfix">


            <li><a href="#"><i class="fa fa-user"></i><?=@$_SESSION['name'][$_SESSION['language']]?></a>
                <ul>
                    <li><a href="<?=base_url('Teachers/profile')?>"><?=translate('my information')?></a></li>
                    <li><a href="<?=translate('student/courses')?>"><?=translate('my courses')?></a></li>
                    <li><a href="<?=base_url('pages/logout')?>"><?=translate('logout')?></a></li>
                </ul>
            </li>
            <li>
                <a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i></a>
            </li>
            <li>
                <a href="" id="btn-offcanvas-menu"><i class="fa fa-bars"></i></a>
            </li>
        </ul>
    </nav>

</nav>