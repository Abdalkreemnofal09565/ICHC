<form id="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4" style="background:#00A7E5 ; margin-top: 10vh ; margin-bottom: 10vh">

                <div class="row text-white text-center">
                    <div class="col-md-12">
                        <br>
                        <br>
                        <b style="font-size: xx-large;  text-transform: uppercase;"
                           class="text-center  text-white"><?= translate('Login') ?></b>
                    </div>
                </div>
                <div class="row bg-grey">
                    <br>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="email" name="email" class="form-control"
                                   style="border-radius: 0px;padding: 5px; "
                                   placeholder="<?= translate('enter email') ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="password"  name="password" class="form-control"
                                   style="border-radius: 0px;padding: 5px; "
                                   placeholder="<?= translate('enter password') ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button id="submit_btn" class="btn btn-primary"
                                    style="width: 100% ; text-transform: uppercase;">
                                <?= translate('submit') ?>
                            </button>
                        </div>
                        <hr style="border: 0;
    height: 2px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0),rgb(0,167,229), rgba(0, 0, 0, 0));
    margin: 15px 0px 15px;">
                        <div class="row" style="padding-bottom: 10px">
                            <div class="col-md-6">
                                <a href=""><?= translate('forget password ? ') ?></a>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href=""><?= translate("don't have account ?") ?></a>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</form>
<script>
    $("#login").submit(function (event) {
        event.preventDefault();
        $("#submit_btn").html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i><?=translate("please wait ..")?>').attr('disabled', 'disabled');
        $.ajax({
            url: "<?=base_url('users/chack_user')?>",
            method: "post",
            data: $(this).serialize(),
            success: function (response) {
                var data = JSON.parse(response);
                if (data.result == 1) {
                    $("#submit_btn").addClass('btn-success').removeClass('btn-primary').removeClass('btn-danger').html("<?=translate('success login')?>");
                    setTimeout(function () {
                        location.href = "<?=base_url()?>";
                    },2000)
                } else if (data.result == "-1") {
                    $("#submit_btn").addClass('btn-danger').removeClass('btn-primary').html("<?=translate('wrong  email or password')?>");
                    setTimeout(function () {
                        $("#submit_btn").addClass('btn-primary').removeClass('btn-danger').removeAttr('disabled').html("<?=translate('submit')?>");

                    }, 4500)
                }else if (data.result == "-2") {
                    $("#submit_btn").addClass('btn-danger').removeClass('btn-primary').html("<?=translate('Account not active')?>");

                    setTimeout(function () {
                        $("#submit_btn").addClass('btn-primary').removeClass('btn-danger').removeAttr('disabled').html("<?=translate('submit')?>");

                    }, 4500)
                }
            }
        })
    })
</script>