<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('template/') ?>images/favicon.png" type="image/x-icon">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <link href="<?= base_url('template/') ?>style.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?= base_url('template/') ?>switcher/demo.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url('template/') ?>switcher/colors/blue.css" type="text/css" id="colors">
    <link rel="stylesheet" href="<?= base_url('template/css/select2.css') ?>">
    <link rel="stylesheet" href="<?= base_url('template/css/select2-bootstrap.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/css/datatable.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/css/summernote.min.css') ?>"/>
    <link rel="stylesheet" href="<?= base_url('template/css/bootstrap-tagsinput.css') ?>">
    <link rel="stylesheet" href="<?= base_url('template/crop/croppie.css') ?>">
    <script src="<?= base_url("template/js/jquery.min.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url('template/js/datatable.js') ?>"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script type="text/javascript" src="<?= base_url("template/js/bootstrap.min.js") ?>"></script>

    <script src="<?= base_url('template/crop/croppie.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('template/js/summernote.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url("template/js/select2.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url("template/js/bootstrap-tagsinput.js") ?>"></script>
    <script type="text/javascript" src="<?= base_url("template/js/bootstrap-tagsinput-angular.js") ?>"></script>

</head>

<body>
<!-- Preload images start //-->
<div class="images-preloader" id="images-preloader">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!-- Preload images end //-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-12">
                        <img style="width: 100%" src="<?= base_url('alert.png') ?>" alt="">
                    </div>
                    <div class="col-md-12 text-center">
                        <br>
                        <h3 class="text-danger" id="message_error">Error</h3>
                        <br>
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <button class="btn btn-primary  " data-dismiss="modal" aria-label="Close"
                                    style="width: 100%;"> <?= translate('close') ?></button>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="wrapper">


    <!-- header close -->
    <div class="gray-line"></div>

    <!-- Modal Search begin -->
    <div id="myModal" class="modal fade" role="dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-dialog myModal-search">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <form role="search" method="get" class="search-form" action="">
                        <input type="search" class="search-field" placeholder="Search here..." value="" title=""/>
                        <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Search close -->

    <!-- Menu OffCanvas right begin -->
    <div class="navright-button hidden-sm">
        <div class="compact-menu-canvas" id="offcanvas-menu">
            <h3>menu</h3><a id="btn-close-canvasmenu"><i class="fa fa-close"></i></a>
            <nav>
                <ul class="clearfix">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="comingsoon.html">Coming Soon</a></li>
                    <li><a href="register.html">Accreditation</a></li>
                    <li><a href="portfolio.html">Portfolios</a></li>
                    <li><a href="article.html">Blog</a></li>
                    <li><a href="trainingbks.html">InfoStore</a></li>
                    <li><a href="contact.html">Contact Us</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <?php
    if (isset($_GET['verify']) && $_GET['verify'] == 1) {
        ?>
        <script>
            $("#message_error").html("<b class='text-primary'><?=translate('You must confirm the account by e-mail')?></b>");
            $('#exampleModal').modal('show');
        </script>


        <?php
    }
    if (isset($_GET['verify']) && $_GET['verify'] == 3) {
        ?>
        <script>
            $("#message_error").html("<b class='text-success'><?=translate('The account has been confirmed')?></b>");
            $('#exampleModal').modal('show');
        </script>


        <?php
    }

    ?>
