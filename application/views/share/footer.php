<script>

    function item_active(id) {
        $.ajax({
            url: "<?=base_url()?>" + name_table + '/active',
            data: {id: id},
            method: "post",
            success: function () {
                table.api().ajax.reload();
            }
        });
    }

    function item_inactive(id) {
        $.ajax({
            url: "<?=base_url()?>" + name_table + '/inactive',
            data: {id: id},
            method: "post",
            success: function () {
                table.api().ajax.reload();
            }
        });
    }

    function delete_item(id) {

        $.ajax({
            url: "<?=base_url()?>" + name_table + '/remove',
            data: {id: id},
            method: "post",
            success: function () {
                // $("#table").DataTable().ajax.reload();
                table.api().ajax.reload();


            }
        });
    }
</script>
<footer class="footer-1 bg-color-1">

    <!-- main footer begin -->
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="compact-widget">
                        <div class="widget-inner">
                            <img class="logo-footer" src="<?= base_url('template/') ?>images/logo-footer.png"
                                 alt="ICHC company">
                            <p>ICHC is a Training and Consulting Center, works with you to HR development, enterprise
                                empowering and to enhance your business.</p>
                            <div class="social-icons clearfix">
                                <a href="https://www.facebook.com/ichc.hr" target="blanc" class="facebook tipped"
                                   data-title="facebook" data-tipper-options='{"direction":"top","follow":"true"}'><i
                                            class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/ichcsy" target="blanc" class="twitter tipped"
                                   data-title="twitter" data-tipper-options='{"direction":"top","follow":"true"}'><i
                                            class="fa fa-twitter"></i></a>
                                <a href="https://plus.google.com/+ICHChr" target="blanc" class="google tipped"
                                   data-title="google +" data-tipper-options='{"direction":"top","follow":"true"}'><i
                                            class="fa fa-google-plus"></i></a>
                                <a href="http://www.youtube.com/c/ICHChr" target="blanc" class="youtube tipped"
                                   data-title="youtube" data-tipper-options='{"direction":"top","follow":"true"}'><i
                                            class="fa fa-youtube-play"></i></a>
                                <a href="https://www.linkedin.com/company/ichc---international-center-for-human-construction"
                                   target="blanc" class="linkedin tipped" data-title="linkedin"
                                   data-tipper-options='{"direction":"top","follow":"true"}'><i
                                            class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="compact-widget">
                        <h3 class="widget-title">Features</h3>
                        <div class="widget-inner">
                            <ul>
                                <li><a href="about.html">About Us</a></li>
                                <li><a href="service.html">Services</a></li>
                                <li><a href="register.html">Register</a></li>
                                <li><a href="portfolio.html">ICHC-Portfolio</a></li>
                                <li><a href="comingsoon.html">Coming Soon</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="compact-widget">
                        <h3 class="widget-title">Contact Us</h3>
                        <div class="widget-inner">
                            <p>Malaysia: +6 032 7138 735/6</p>
                            <p>Syria: +963 9949 333 71</p>
                            <p>Oman: +968 92 843 490</p>
                            <p>Turkey: +905 31 20 84 637</p>
                            <p>Algeria: +213 79 38 47 404</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="compact-widget">
                        <h3 class="widget-title">Newsletter</h3>
                        <div class="widget-inner">
                            <div class="newsletter newsletter-widget">
                                <p>Stay informed about our news and events</p>
                                <form action="" method="post">
                                    <p><input class="newsletter-email" type="email" name="email"
                                              placeholder="Your email"><i class="fa fa-envelope-o"></i></p>
                                    <p><input class="newsletter-submit" type="submit" value="Subscribe"></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    Copyright &copy; 2017 Designed by <a href="http://www.hasorc.com/" target="blanc">HASORC</a>. All
                    rights reserved.
                </div>
            </div>
        </div>
    </div>
</footer>
</div>


<a id="to-the-top"><i class="fa fa-angle-up"></i></a>
<script src="<?= base_url("template") ?>/js/imagesloaded.pkgd.min.js"></script>
<script src="<?= base_url("template") ?>/js/easing.js"></script>
<script src="<?= base_url("template") ?>/js/owl.carousel.js"></script>
<script src="<?= base_url("template") ?>/js/jquery.fitvids.js"></script>
<script src="<?= base_url("template") ?>/js/wow.min.js"></script>
<script src="<?= base_url("template") ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url("template") ?>/js/jquery.waypoints.min.js"></script>
<script src="<?= base_url("template") ?>/js/sticky.min.js"></script>
<script src="<?= base_url("template") ?>/js/tipper.js"></script>
<script src="<?= base_url("template") ?>/js/compact.js"></script>
<script src="<?= base_url("template") ?>/js/custom-index1.js"></script>
<script type="text/javascript" src="<?= base_url("template/") ?>rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript"
        src="<?= base_url("template/") ?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url("template/") ?>js/revslider-custom.js"></script>

</body>
</html>
