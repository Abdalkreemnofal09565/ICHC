<style>
.window-checkbox{
    border: 1px grey solid; padding: 10px ;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <br>
                <br>
                <br>
                <div class="col-md-3">

                    <div class="list-group">
                        <a id="base" style="cursor: pointer"
                           class="list-group-item active"><?= translate('Base information') ?></a>
                        <a id="more" style="cursor: pointer"
                           class="list-group-item"><?= translate('More information') ?></a>
                        <a id="security" style="cursor: pointer"
                           class="list-group-item"><?= translate('Security') ?></a>
                        <a id="connect"
                                                                                       style="cursor: pointer"
                                                                                       class="list-group-item"><?= translate('connect information') ?></a>

                    </div>
                </div>
                <div class="col-md-9">
                    <form id="form_base">
                        <h3><?= translate('base information') ?></h3>
                        <hr>
                        <?php
                        create_form_group('email', 'email', 'email', 'email', 'email', $user['u_email']);
                        create_form_group('arabic name', 'ar_name', 'ar_name', 'arabic name', 'text', $user['u_name_ar']);
                        create_form_group('english name', 'en_name', 'en_name', 'english name', 'text', $user['u_name_en']);
                        create_form_group('mobile', 'mobile', 'mobile', '+963900000000', 'text', $user['u_mobile']);
                        create_form_group('telephone', 'telephone', 'telephone', '+963911000000', 'text', $user['u_telephone']);

                        ?>
                        <div class="form-group">
                            <lable for="county"><?= translate('county') ?></lable>
                            <select class="form-control" name="" id="country"></select>
                        </div>
                        <div class="form-group">
                            <lable for="city"><?= translate('city') ?></lable>
                            <select class="form-control" name="" id="city"></select>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary"><?= translate('save') ?></button>
                        </div>
                        <br>
                        <br>
                        <br>
                    </form>
                    <form id="form_more">
                        <h3><?= translate('more information') ?></h3>
                        <hr>
                        <?php
                        create_form_group('houre price', 'hp', 'hp', 'houre price', 'number', $teacher['t_houre_price']);
                        create_form_group('birthdate', 'birthdate', 'birthdate', 'birthdate', 'date', $teacher['t_houre_price']);
                        ?>
                        <div class="form-group window-checkbox " >
                            <label class="kt-checkbox">
                                <input type="checkbox" > <span style="margin-left: 10px ; margin-right: 10px"><?= translate('i have certificate training from ICHC') ?></span>
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group window-checkbox">
                            <label class="kt-checkbox">
                                <input type="checkbox"><span style="margin-left: 10px ; margin-right: 10px"> <?= translate('i have certificate apprecaiation from ICHC') ?></span>
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group window-checkbox">
                            <label class="kt-checkbox">
                                <input type="checkbox"> <span style="margin-left: 10px ; margin-right: 10px"><?= translate('i am previously trained  with ICHC') ?></span>
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group window-checkbox">
                            <label class="kt-checkbox">
                                <input type="checkbox"> <span style="margin-left: 10px ; margin-right: 10px"><?= translate('i can  training without my city') ?></span>
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group window-checkbox">
                            <label class="kt-checkbox">
                                <input type="checkbox"><span style="margin-left: 10px ; margin-right: 10px"> <?= translate('i can  training without my country') ?></span>
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group window-checkbox">
                            <label class="kt-checkbox">
                                <input type="checkbox"><span style="margin-left: 10px ; margin-right: 10px"> <?= translate('i can  training without my country') ?></span>
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group window-checkbox">
                            <label class="kt-checkbox">
                                <input type="checkbox"><span style="margin-left: 10px ; margin-right: 10px"> <?= translate('i can  training without my country') ?></span>
                                <span></span>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
 $(document).ready(function () {
     $("#country").select2({
         theme: "bootstrap",
         ajax: {
             url: "<?=base_url('pages/country_list/' . $user['country'])?>",
             dataType: 'json',
             type: "post",

             processResults: function (data) {
                 // Transforms the top-level key of the response object from 'items' to 'results'
                 return {
                     results: data
                 };
             }
         },


     });
     $("#country").change(function () {
         $("#city").select2({
             theme: "bootstrap",
             ajax: {
                 url: "<?=base_url('pages/city_list/')?>" + $("#country").val(),
                 dataType: 'json',
                 type: "post",
                 processResults: function (data) {
                     // Transforms the top-level key of the response object from 'items' to 'results'
                     return {
                         results: data
                     };
                 }
             }
         });
     });
     $(".list-group-item").click(function () {
         $(".list-group-item").removeClass('active');
         $(this).addClass('active');
         if (this.id == "base") {
             $("#form_more").hide();
             $("#form_base").show();
         } else if (this.id == "more") {
             $("#form_more").show();
             $("#form_base").hide();
         }
     });
     $("#form_more").show();
     $("#form_base").hide();
 });
</script>