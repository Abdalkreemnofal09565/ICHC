<?php
require 'Pages.php';

class Admin extends Pages
{
    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['user_type']) || (isset($_SESSION['user_type']) && $_SESSION['user_type'] != 4)) {
            redirect(base_url('pages/login'));
        }
    }

    // TODO : Import file excel for teachers , trining centers  , courses with student

    function mange_website()
    {

        $this->render('admin/website_mange', 'website mange');
    }

    function students($para = null, $para1 = null)
    {
        $this->load->model('Student_model', 'model');
        if (!isset($para)) {
            $data['datatable'] = base_url('Admin/students/datatable');
            $this->render('admin/student/index', 'Student', $data);
        }
        else {
            if ($para == "datatable") {

                echo $this->model->datatable();
            }
            elseif ($para == "new") {
                $this->render('admin/student/new', 'New training center');

            }
            elseif ($para == "add") {
                $this->form_validation->set_rules('name_ar', translate('name_ar'), 'required|min_length[2]');
                $this->form_validation->set_rules('name_en', translate('name_en'), 'required');
                $this->form_validation->set_rules('email', translate('email'), 'required|is_unique[users.u_email]');
                $this->form_validation->set_rules('mobile', translate('mobile'), 'required');
                $this->form_validation->set_rules('country', translate('countries'), 'required');
                $this->form_validation->set_rules('city', translate('city'), 'required');
                $this->form_validation->set_rules('gender', translate('gender'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    $user['u_name_ar'] = $this->input->post('name_ar');
                    $user['u_name_en'] = $this->input->post('name_en');
                    $user['u_email'] = $this->input->post('email');
                    $user['u_mobile'] = $this->input->post('mobile');
                    $user['u_telephone'] = $this->input->post('telephone');
                    $user['gender'] = $this->input->post('gender');
                    $user['country'] = $this->input->post('country');
                    $user['city'] = $this->input->post('city');
                    $user['state'] = 1;
                    //for student
                    $user['user_type'] = 1;
                    $this->load->model('User_model', 'muser');
                    $user_id = $this->muser->base_add($user);
                    $student['s_birthdate'] = $this->input->post('birthday');
                    $student['s_facebook'] = $this->input->post('facebook');
                    $student['s_additional_information'] = $this->input->post('about');
                    $student['user'] = $user_id;
                    $this->model->base_add($student);
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $result = array('result' => 0);
                    }
                    else {
                        $result = array('result' => 1);
                    }
                    echo json_encode($result);
                }
                else {
                    $data['name_ar'] = form_error('name_ar');
                    $data['name_en'] = form_error('name_en');
                    $data['email'] = form_error('email');
                    $data['mobile'] = form_error('mobile');
                    $data['countries'] = form_error('countries');
                    $data['gender'] = form_error('gender');
                    $data['city'] = form_error('city');
                    $data['result'] = 0;
                    echo json_encode($data);

                }
            }
            elseif ($para == "import") {
                $this->render('admin/student/import_file', 'Import by Excel');
            }
            elseif ($para == "do_import") {
                $this->load->library('excel');

                if (isset($_FILES["file"]["name"])) {
                    $path = $_FILES["file"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach ($object->getWorksheetIterator() as $worksheet) {
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        if ($highestRow == 1) {
                            $this->load->view('admin/empty_file');
                            return;
                        }
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $name_ar = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $name_en = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $mobile = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                            $telephone = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $email = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $gender = $worksheet->getCellByColumnAndRow(5, $row)->getValue();

                            $birthday = date('Y-m-d', strtotime(trim($worksheet->getCellByColumnAndRow(6, $row)->getValue())));
//                            $birthday = date('d.m.Y', strtotime(str_replace('/', '-', trim((string)) $worksheet->getCellByColumnAndRow(6, $row)->getValue())));
                            $facebook = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                            $country = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                            $city = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                            $data[] = array(
                                'name_ar' => $name_ar,
                                'name_en' => $name_en,
                                'mobile' => $mobile,
                                'telephone' => $telephone,
                                'email' => $email,
                                'gender' => $gender,
                                'birthday' => $birthday,
                                'facebook' => $facebook,
                                'country' => $country,
                                'city' => $city,
                            );
                        }
                        $data['data'] = $data;
                        $this->load->view('admin/student/import_excel', $data);

                    }

                }
                else {
                    $this->load->view('admin/empty_file');

                }

            }
            elseif ($para == "accepte_import") {
                $this->form_validation->set_rules('name_ar', translate('name_ar'), 'required|min_length[2]');
                $this->form_validation->set_rules('name_en', translate('name_en'), 'required');
                $this->form_validation->set_rules('email', translate('email'), 'required|is_unique[users.u_email]');
                $this->form_validation->set_rules('mobile', translate('mobile'), 'required');
                $this->form_validation->set_rules('country', translate('countries'), 'required');
                $this->form_validation->set_rules('city', translate('city'), 'required');
                $this->form_validation->set_rules('gender', translate('gender'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    $user['u_name_ar'] = $this->input->post('name_ar');
                    $user['u_name_en'] = $this->input->post('name_en');
                    $user['u_email'] = $this->input->post('email');
                    $user['u_mobile'] = $this->input->post('mobile');
                    $user['u_telephone'] = $this->input->post('telephone');
                    $user['gender'] = $this->input->post('gender');
                    $user['country'] = $this->input->post('country');
                    $user['city'] = $this->input->post('city');
                    $user['state'] = 1;
                    //for student
                    $user['user_type'] = 1;
                    $this->load->model('User_model', 'muser');
                    $user_id = $this->muser->base_add($user);
                    $student['s_birthdate'] = $this->input->post('birthday');
                    $student['s_facebook'] = $this->input->post('facebook');
                    $student['user'] = $user_id;
                    $this->model->base_add($student);
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $result = array('result' => 0);
                    }
                    else {
                        $result = array('result' => 1);
                    }
                    echo json_encode($result);
                }
                else {
                    $data['name_ar'] = form_error('name_ar');
                    $data['name_en'] = form_error('name_en');
                    $data['email'] = form_error('email');
                    $data['mobile'] = form_error('mobile');
                    $data['countries'] = form_error('countries');
                    $data['gender'] = form_error('gender');
                    $data['city'] = form_error('city');
                    $data['result'] = 0;
                    echo json_encode($data);

                }
            }
            elseif ($para == "download_template") {
                $this->load->helper('download');
                force_download('excel/student_template.xlsx', NULL);
            }
            elseif ($para == "edit" && isset($para1)) {
                $this->load->model('User_model');
                $data['user'] = $this->User_model->get($para1);
                $data['student'] = $this->model->get_by_user($para1);
                $data['user_id'] = $para1;
                $this->render('admin/student/edit', 'Edit Student', $data);
            }
            elseif ($para == "show" && isset($para1)) {
                $this->load->model('User_model');
                $data['user'] = $this->User_model->get($para1);
                $data['student'] = $this->model->get_by_user($para1);
                $data['user_id'] = $para1;
                $this->render('admin/student/show', 'show information', $data);
            }
            elseif ($para == "do_edit") {
                $this->form_validation->set_rules('name_ar', translate('name_ar'), 'required|min_length[2]');
                $this->form_validation->set_rules('name_en', translate('name_en'), 'required');
                $this->form_validation->set_rules('email', translate('email'), 'required');
                $this->form_validation->set_rules('mobile', translate('mobile'), 'required');
                $this->form_validation->set_rules('country', translate('countries'), 'required');
                $this->form_validation->set_rules('city', translate('city'), 'required');
                $this->form_validation->set_rules('gender', translate('gender'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    $user['u_name_ar'] = $this->input->post('name_ar');
                    $user['u_name_en'] = $this->input->post('name_en');
                    $user['u_email'] = $this->input->post('email');
                    $user['u_mobile'] = $this->input->post('mobile');
                    $user['u_telephone'] = $this->input->post('telephone');
                    $user['gender'] = $this->input->post('gender');
                    $user['country'] = $this->input->post('country');
                    $user['city'] = $this->input->post('city');
//                    $user['state'] = 1;
//                    //for student
//                    $user['user_type'] = 1;
                    $this->load->model('User_model', 'muser');
                    $user_id = $this->muser->base_update($this->post('id'), $user);
                    $student['s_birthdate'] = $this->input->post('birthday');
                    $student['s_facebook'] = $this->input->post('facebook');
                    $student['s_additional_information'] = $this->input->post('about');

                    $student_id = $this->db->select('s_id as id ')->get_where('student', array('user' => $this->post('id')))->row()->id;
                    $this->model->base_update($student_id, $student);
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $result = array('result' => 0);
                    }
                    else {
                        $result = array('result' => 1);
                    }
                    echo json_encode($result);
                }
                else {
                    $data['name_ar'] = form_error('name_ar');
                    $data['name_en'] = form_error('name_en');
                    $data['email'] = form_error('email');
                    $data['mobile'] = form_error('mobile');
                    $data['countries'] = form_error('countries');
                    $data['gender'] = form_error('gender');
                    $data['city'] = form_error('city');
                    $data['result'] = 0;
                    echo json_encode($data);

                }
            }
        }
    }

    function teachers($para = null, $para1 = null)
    {
        $this->load->model('Teacher_model', 'model');
        if (!isset($para)) {
            $data['datatable'] = base_url('Admin/teachers/datatable');
            $this->render('admin/teachers/index', 'teachers', $data);
        }
        else {
            if ($para == "edit" && isset($para1)) {
                $this->load->model('User_model');
                $data['user'] = $this->User_model->get($para1);
                $data['teacher'] = $this->model->get_by_user($para1);
                $data['user_id'] = $para1;
                $this->render('admin/teachers/edit', 'Edit Teacher', $data);
            }
            if ($para == "datatable") {

                echo $this->model->datatable();
            }
            elseif ($para == "new") {
                $this->render('admin/teachers/new', 'New training center');

            }
            elseif ($para == "do_edit") {
                $this->form_validation->set_rules('name_ar', translate('name_ar'), 'required|min_length[2]');
                $this->form_validation->set_rules('name_en', translate('name_en'), 'required');
                $this->form_validation->set_rules('email', translate('email'), 'required');
                $this->form_validation->set_rules('mobile', translate('mobile'), 'required');
                $this->form_validation->set_rules('country', translate('countries'), 'required');
                $this->form_validation->set_rules('city', translate('city'), 'required');
                $this->form_validation->set_rules('id', translate('id'), 'required');
                $this->form_validation->set_rules('gender', translate('gender'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    $user['u_name_ar'] = $this->input->post('name_ar');
                    $user['u_name_en'] = $this->input->post('name_en');
                    $user['u_email'] = $this->input->post('email');
                    $user['u_mobile'] = $this->input->post('mobile');
                    $user['u_telephone'] = $this->input->post('telephone');
                    $user['gender'] = $this->input->post('gender');
                    $user['country'] = $this->input->post('country');
                    $user['city'] = $this->input->post('city');
                    $user['state'] = 1;
                    $user['user_type'] = 3;
                    $this->load->model('User_model', 'm_user');
                    $user_id = $this->m_user->base_update($this->post('id'), $user);

                    $teacher['t_birthdate'] = $this->post('birthdate');
                    $teacher['t_houre_price'] = $this->post('price_hour');

                    $teacher['t_additional_information'] = $this->post('additional_information');
                    if ($this->post('certificate_training')) {
                        $teacher['t_certificate_training'] = 1;
                    }
                    if ($this->post('certificate_appreciation')) {
                        $teacher['t_certificate_appreciation'] = 1;
                    }
                    if ($this->post('previously_trained')) {
                        $teacher['t_previously_trained'] = 1;
                    }
                    if ($this->post('training_out_country')) {
                        $teacher['t_training_out_country'] = 1;
                    }
                    if ($this->post('training_out_city')) {
                        $teacher['t_training_out_city'] = 1;
                    }
                    if ($this->post('certified')) {
                        $teacher['tـcertified'] = 1;
                    }
//                    $teacher['user'] = $user_id;
                    $teacher_id = $this->db->select('t_id as id ')->get_where('teachers', array('user' => $this->post('id')))->row()->id;
                    $this->model->base_update($teacher_id, $teacher);
//                    $this->model->base_add($teacher);
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $result = array('result' => 0);
                    }
                    else {
                        $result = array('result' => 1);
                    }
                    echo json_encode($result);
                }
                else {
                    $data['name_ar'] = form_error('name_ar');
                    $data['name_en'] = form_error('name_en');
                    $data['email'] = form_error('email');
                    $data['mobile'] = form_error('mobile');
                    $data['countries'] = form_error('countries');
                    $data['gender'] = form_error('gender');
                    $data['city'] = form_error('city');
                    $data['result'] = 0;
                    echo json_encode($data);

                }
            }
            elseif ($para == "add") {
                $this->form_validation->set_rules('name_ar', translate('name_ar'), 'required|min_length[2]');
                $this->form_validation->set_rules('name_en', translate('name_en'), 'required');
                $this->form_validation->set_rules('email', translate('email'), 'required|is_unique[users.u_email]');
                $this->form_validation->set_rules('mobile', translate('mobile'), 'required');
                $this->form_validation->set_rules('country', translate('countries'), 'required');
                $this->form_validation->set_rules('city', translate('city'), 'required');
                $this->form_validation->set_rules('gender', translate('gender'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    $user['u_name_ar'] = $this->input->post('name_ar');
                    $user['u_name_en'] = $this->input->post('name_en');
                    $user['u_email'] = $this->input->post('email');
                    $user['u_mobile'] = $this->input->post('mobile');
                    $user['u_telephone'] = $this->input->post('telephone');
                    $user['gender'] = $this->input->post('gender');
                    $user['country'] = $this->input->post('country');
                    $user['city'] = $this->input->post('city');
                    $user['state'] = 1;
                    $user['user_type'] = 3;
                    $this->load->model('User_model', 'm_user');
                    $user_id = $this->m_user->base_add($user);

                    $teacher['t_birthdate'] = $this->post('birthdate');
                    $teacher['t_additional_information'] = $this->post('additional_information');
                    $teacher['t_houre_price'] = $this->post('price_hour');
                    if ($this->post('certificate_training')) {
                        $teacher['t_certificate_training'] = 1;
                    }
                    if ($this->post('certificate_appreciation')) {
                        $teacher['t_certificate_appreciation'] = 1;
                    }
                    if ($this->post('previously_trained')) {
                        $teacher['t_previously_trained'] = 1;
                    }
                    if ($this->post('training_out_country')) {
                        $teacher['t_training_out_country'] = 1;
                    }
                    if ($this->post('training_out_city')) {
                        $teacher['t_training_out_city'] = 1;
                    }
                    if ($this->post('certified')) {
                        $teacher['tـcertified'] = 1;
                    }
                    $teacher['user'] = $user_id;
                    $this->model->base_add($teacher);
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $result = array('result' => 0);
                    }
                    else {
                        $result = array('result' => 1);
                    }
                    echo json_encode($result);
                }
                else {
                    $data['name_ar'] = form_error('name_ar');
                    $data['name_en'] = form_error('name_en');
                    $data['email'] = form_error('email');
                    $data['mobile'] = form_error('mobile');
                    $data['countries'] = form_error('countries');
                    $data['gender'] = form_error('gender');
                    $data['city'] = form_error('city');
                    $data['result'] = 0;
                    echo json_encode($data);

                }
            }
            elseif ($para == "import") {
                $this->render('admin/teachers/import_file', 'Import By excel');
            }
            elseif ($para == "download_template") {
                $this->load->helper('download');
                force_download('excel/teacher_template.xlsx', NULL);
            }
            elseif ($para == "do_import") {

                $this->load->library('excel');

                if (isset($_FILES["file"]["name"])) {
                    $path = $_FILES["file"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach ($object->getWorksheetIterator() as $worksheet) {
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        if ($highestRow == 1) {
                            $this->load->view('admin/empty_file');
                            return;
                        }
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $name_ar = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $name_en = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $mobile = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                            $telephone = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $email = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $gender = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $birthday = date('Y-m-d', strtotime(trim($worksheet->getCellByColumnAndRow(6, $row)->getValue())));
                            $facebook = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                            $country = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                            $city = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                            $certificate_training = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                            $certificate_appreciation = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                            $previously_trained = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                            $training_out_country = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                            $training_out_city = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                            $certified = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                            $price_hours = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                            if ($certificate_training == "yes") {
                                $certificate_training = 1;
                            }
                            else {
                                $certificate_training = 0;
                            }
                            if ($certificate_appreciation == "yes") {
                                $certificate_appreciation = 1;
                            }
                            else {
                                $certificate_appreciation = 0;
                            }
                            if ($previously_trained == "yes") {
                                $previously_trained = 1;
                            }
                            else {
                                $previously_trained = 0;
                            }
                            if ($training_out_country == "yes") {
                                $training_out_country = 1;
                            }
                            else {
                                $training_out_country = 0;
                            }
                            if ($training_out_city == "yes") {
                                $training_out_city = 1;
                            }
                            else {
                                $training_out_city = 0;
                            }
                            if ($certified == "yes") {
                                $certified = 1;
                            }
                            else {
                                $certified = 0;
                            }


                            $data[] = array(
                                'name_ar' => $name_ar,
                                'name_en' => $name_en,
                                'mobile' => $mobile,
                                'telephone' => $telephone,
                                'email' => $email,
                                'gender' => $gender,
                                'birthday' => $birthday,
                                'facebook' => $facebook,
                                'country' => $country,
                                'city' => $city,
                                'certificate_training' => $certificate_training,
                                'certificate_appreciation' => $certificate_appreciation,
                                'previously_trained' => $previously_trained,
                                'training_out_country' => $training_out_country,
                                'training_out_city' => $training_out_city,
                                'certified' => $certified,
                                'price' => $price_hours
                            );
                        }
                        $data['data'] = $data;
                        $this->load->view('admin/teachers/import_excel', $data);

                    }

                }
                else {
                    $this->load->view('admin/empty_file');
                }

            }
            elseif ($para == "active") {
                $this->db->where('u_id', $this->post('id'))->update('users', array('state' => 1));
            }
            elseif ($para == "remove") {
                $this->db->where('u_id', $this->post('id'))->update('users', array('state' => 3));
            }
            elseif ($para == "inactive") {
                $this->db->where('u_id', $this->post('id'))->update('users', array('state' => 2));
            }
            elseif ($para == "show" && isset($para1)) {
                $this->load->model('User_model');
                $data['user'] = $this->User_model->get($para1);
                $data['teacher'] = $this->model->get_by_user($para1);
                $data['courses'] = $this->model->get_courses($data['teacher']['t_id']);
//        print_r($data);
                $this->render('admin/teachers/show', 'Information', $data);
            }

        }
    }

    function training_center($para = null)
    {
        $this->load->model('Training_center_model', 'model');
        if (!isset($para)) {
            $data['datatable'] = base_url('Admin/training_center/datatable');
            $this->render('admin/training_center/index', 'training_center', $data);
        }
        else {
            if ($para == "datatable") {
                echo $this->model->datatable();
            }
            elseif ($para == "new") {
                $this->render('admin/training_center/new', 'New training center');
            }
            elseif ($para == "add") {
                $this->form_validation->set_rules('name_ar', translate('name_ar'), 'required|min_length[2]');
                $this->form_validation->set_rules('name_en', translate('name_en'), 'required');
                $this->form_validation->set_rules('email', translate('email'), 'required');
                $this->form_validation->set_rules('mobile', translate('mobile'), 'required');
                $this->form_validation->set_rules('country', translate('countries'), 'required');
                $this->form_validation->set_rules('city', translate('city'), 'required');
                $this->form_validation->set_rules('gender', translate('gender'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $this->db->trans_start();
                    $user['u_name_ar'] = $this->input->post('name_ar');
                    $user['u_name_en'] = $this->input->post('name_en');
                    $user['u_email'] = $this->input->post('email');
                    $user['u_mobile'] = $this->input->post('mobile');
                    $user['u_telephone'] = $this->input->post('telephone');
                    $user['gender'] = $this->input->post('gender');
                    $user['country'] = $this->input->post('country');
                    $user['city'] = $this->input->post('city');
                    $user['state'] = 1;
                    $user['user_type'] = 3;
                    $this->load->model('User_model', 'muser');
                    $user_id = $this->muser->base_add($user);
                    $training['tc_connect'] = $this->input->post('connect');
                    $training['tc_attr'] = $this->input->post('attr');
                    $training['tc_location'] = $this->input->post('location');
                    $training['tc_cost_day'] = $this->input->post('cost_day');
                    $training['tc_cost_hours'] = $this->input->post('cost_hours');
                    $training['tc_note'] = $this->input->post('note');
                    if (isset($_POST['certified']) && $_POST['certified'] == "on") {
                        $training['certified'] = 1;
                    }
                    $training['user'] = $user_id;
                    $this->model->base_add($training);
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $result = array('result' => 0);
                    }
                    else {
                        $result = array('result' => 1);
                    }
                    echo json_encode($result);
                }
                else {
                    $data['name_ar'] = form_error('name_ar');
                    $data['name_en'] = form_error('name_en');
                    $data['email'] = form_error('email');
                    $data['mobile'] = form_error('mobile');
                    $data['countries'] = form_error('countries');
                    $data['gender'] = form_error('gender');
                    $data['city'] = form_error('city');
                    $data['result'] = 0;
                    echo json_encode($data);

                }
            }
        }
    }

    function maters($para = null)
    {
        $this->load->model('Mater_model', 'model');
        if (!isset($para)) {
            $data['datatable'] = base_url('admin/maters/datatable');
            $this->render('admin/maters/index', 'Maters', $data);
        }
        else {
            if ($para == "datatable") {
                echo $this->model->datatable();
            }
            elseif ($para == "new") {
                $this->render('admin/maters/new', 'New Mater');
            }
            elseif ($para == "add") {
                $this->form_validation->set_rules('title', translate('title maters'), 'required|min_length[2]');
                $this->form_validation->set_rules('hours', translate('count hours'), 'required');
                $this->form_validation->set_rules('about', translate('about maters'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $data['m_title'] = $this->input->post('title');
                    $data['m_count_hours'] = $this->input->post('hours');
                    if (isset($_POST['certified']) && $_POST['certified'] == "on")
                        $data['m_certified'] = 1;
                    else
                        $data['m_certified'] = 0;
                    $data['m_about'] = $this->input->post('about');
                    $data['m_scientific_content'] = $this->input->post('scientific_content');
                    $data['m_scientific_requirement'] = $this->input->post('scientific_requirement');
                    $data['m_categories'] = $this->input->post('categories');
                    $data['m_more_info'] = $this->input->post('more_info');
                    $data['m_references'] = $this->input->post('references');
                    $id = $this->model->base_add($data);
                    echo json_encode(array('result' => 1, 'id' => $id));
                }
                else {
                    echo json_encode(array('result' => 0,
                        'title' => form_error('title'),
                        'hours' => form_error('hours'),
                        'about' => form_error('about'),

                    ));
                }
            }
        }
    }

    function course($para = null, $para2 = null, $para3 = null)
    {
        $this->load->model('Course_model', 'model');

        if (!isset($para)) {
            $data['datatable'] = base_url('admin/course/datatable');
            $this->render('admin/course/index', 'Course ', $data);
        }
        else {
            if ($para == "datatable") {
                echo $this->model->datatable();
            }
            elseif ($para == "new") {
                $this->render('admin/course/new', 'New Mater');
            }
            elseif ($para == "add") {
                $this->form_validation->set_rules('title', translate('title '), 'required|min_length[2]');
                $this->form_validation->set_rules('hours', translate('count hours'), 'required');
                $this->form_validation->set_rules('cost', translate('Cost'), 'required');
                $this->form_validation->set_rules('city', translate('city'), 'required');
                if ($this->form_validation->run() == TRUE) {
                    $data['c_title'] = $this->input->post('title');
                    $data['c_location'] = $this->input->post('location');
                    $data['c_cost'] = $this->input->post('cost');
                    $data['c_more_info'] = $this->input->post('more');
                    $data['c_count_hours'] = $this->input->post('hours');
                    $data['city'] = $this->input->post('city');
                    $data['c_maters'] = json_encode($_POST['maters']);
                    $id = $this->model->base_add($data);
                    echo json_encode(array('result' => 1, 'id' => $id));
                }
                else {
                    echo json_encode(array('result' => 0,
                        'title' => form_error('title'),
                        'hours' => form_error('hours'),
                        'cost' => form_error('cost'),
                        'city' => form_error('city'),

                    ));
                }
            }
            elseif ($para == "teachers" && isset($para2)) {
                if ($para2 == "remove") {
                    $this->model->model->teacher_delete($this->post('id'));
                }
                else {

                    $data['datatable'] = base_url('admin/course/datatable_teachers/' . $para2);
                    $this->load->model('Teacher_model', 'Teachers');
                    $data['teachers'] = $this->Teachers->get_list();
                    $data['id'] = $para2;

                    $this->render('admin/course/teachers', 'teachers', $data);
                }

            }
            elseif ($para == "training_center" && isset($para2)) {
                if ($para2 == "add") {
                    $data['course'] = $this->post('course');
                    $data['training_center'] = $this->post('training_center');
                    $check = $this->db->get_where('course_training_center', $data);
                    if ($check->num_rows() == 0)
                        $this->db->insert('course_training_center', $data);
                    echo json_encode(array('result' => 1));
                }
                elseif ($para2 == "datatable") {
                    echo $this->model->training_center($para3);
                }
                elseif ($para2 == "remove") {
                    $this->model->training_center_delete($this->post('id'));
                }
                elseif (isset($para2)) {
                    $data['id'] = $para2;
                    $data['datatable'] = base_url('admin/course/training_center/datatable/' . $para2);
                    $this->load->model('Training_center_model');
                    $data['Training_centers'] = $this->Training_center_model->get_list();
//                    print_r($data);
                    $this->render('admin/course/training_center', 'training center', $data);

                }
            }
            elseif ($para == "add_teachers" && isset($para2)) {
                $data['course'] = $this->post('course');
                $data['teacher'] = $this->post('teacher');
                $check = $this->db->get_where('course_teachers', $data);
                if ($check->num_rows() == 0)
                    $this->db->insert('course_teachers', $data);
                echo json_encode(array('result' => 1));
            }
            elseif ($para == "datatable_teachers" && isset($para2)) {
                echo $this->model->teachers($para2);
            }
            elseif ($para == "students" && isset($para2)) {
                if ($para2 == "add") {
                    $data['course'] = $this->post('course');
                    $data['student'] = $this->post('student');
                    $check = $this->db->get_where('course_student', $data);
                    if ($check->num_rows() == 0)
                        $this->db->insert('course_student', $data);
                    echo json_encode(array('result' => 1));
                }
                elseif ($para2 == "datatable") {
                    echo $this->model->students($para3);
                }
                elseif ($para2 == "remove") {
                    $this->model->student_delete($this->post('id'));
                }
                elseif ($para2 == "get_mark") {
                    $this->form_validation->set_rules('id', 'id', 'required');
                    if ($this->form_validation->run() == TRUE or FALSE) {
                        $result['result'] = 1;
                        $student = $this->db->where('cs_id', $this->post('id'))->join('student', 'student.s_id=course_student.student')->join('users', 'student.user=users.u_id')->get('course_student');
                        if ($student->num_rows() == 0) {
                            echo json_encode(array('result' => 0));

                        }
                        else {

                            $student = $student->row();
                            $result['student_name'] = '( ' . $student->u_name_ar . ' ' . $student->u_name_en . ' )';
                            $result['mark'] = $student->cs_examـmark;
                            echo json_encode($result);
                        }

                    }
                    else {
                        echo json_encode(array('result' => 0));
                    }
                }
                elseif ($para2 == "set_mark") {
                    $this->form_validation->set_rules('student_id', 'id', 'required');
                    $this->form_validation->set_rules('mark', translate('mark'), 'required');
                    if ($this->form_validation->run() == TRUE or FALSE) {
                        $this->db->where('cs_id', $this->post('student_id'))->update('course_student', array('cs_examـmark' => $this->post('mark')));
                        echo json_encode(array('result' => 1));
                    }
                    else {
                        echo json_encode(array('result' => 0, 'mark' => form_error('mark')));
                    }
                }
                elseif (isset($para2)) {
                    $data['id'] = $para2;
                    $data['datatable'] = base_url('admin/course/students/datatable/' . $para2);
                    $this->load->model('Student_model');
                    $data['students'] = $this->Student_model->get_list();
                    $this->render('admin/course/student', 'Students', $data);

                }
            }
            elseif ($para == "import") {
                $this->render('admin/course/import_file', 'Import By excel');
            }
            elseif ($para == "download_template") {
                $this->load->helper('download');
                force_download('excel/course_template.xlsx', NULL);
            }
            elseif ($para == "do_import") {

                $this->load->library('excel');

                if (isset($_FILES["file"]["name"])) {
                    $path = $_FILES["file"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach ($object->getWorksheetIterator() as $worksheet) {
                        $highestRow = $worksheet->getHighestRow();
                        $highestColumn = $worksheet->getHighestColumn();
                        if ($highestRow == 1) {
                            $this->load->view('admin/empty_file');
                            return;
                        }
                        for ($row = 2; $row <= $highestRow; $row++) {
                            $title = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $count_hours = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $cost = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                            $more_information = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $country = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $city = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $location = date('Y-m-d', strtotime(trim($worksheet->getCellByColumnAndRow(6, $row)->getValue())));
                            $maters = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                            $maters = explode(',', $maters);
                            $maters_data = array();
                            foreach ($maters as $mater) {
                                $mater_value = $this->db->where('m_title', $mater)->get('mater');
                                if ($mater_value->num_rows() == 0) {
                                    $this->db->insert('mater', array('m_title' => $mater));
                                    $maters_data[] = $this->db->insert_id();
                                }
                                else {
                                    $maters_data[] = $mater_value->row()->m_id;
                                }
                            }
                            $data[] = array(
                                'title' => $title,
                                'count_hours' => $count_hours,
                                'cost'=>$cost,
                                'more_info'=>$more_information,
                                'country'=>$country,
                                'city'=>$city,
                                'location'=>$location,
                                'maters'=>$maters_data
                            );
                        }
//                        print_r($data);
                        $data['data'] = $data;
                        $this->load->view('admin/course/import_excel', $data);

                    }

                }
                else {
                    $this->load->view('admin/empty_file');
                }

            }
        }
    }


}