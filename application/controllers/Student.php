<?php
require_once 'Pages.php';

class Student extends Pages
{
    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['user_type']) || (isset($_SESSION['user_type']) && $_SESSION['user_type'] != 1)) {
            redirect(base_url('pages/login'));
        }
    }

    function courses()
    {
        $this->load->model('Student_model', 'model');
        $courses = $this->model->get_couses_info($this->session->userdata('id'));
        if (count($courses) != 0) {
            $data['courses'] = $courses;
            $this->render('student/course/index', 'My courses', $data);
        }
        else {
            $this->render('student/course/empty', 'My courses');
        }
    }

    function index()
    {
        $id = $this->session->userdata('id');
        $this->load->model('User_model');
        $this->load->model('Student_model');
        $data['user'] = $this->User_model->get($id);
        $data['student'] = $this->Student_model->get_by_user($id);
        $social_medias = $this->db->get('social_media')->result();
        $social = array();
        foreach ($social_medias as $social_media) {
            $value = $this->User_model->get_social_media($id, $social_media->sm_id);
            $social [] = array('value' => $value, 'social_id' => $social_media->sm_id, 'social_name' => $social_media->sm_title);
        }
        $data['social'] = $social;
        $this->render('student/profile/index', 'My profile', $data);
    }

    function change_description()
    {
        $this->form_validation->set_rules('description', translate('description'), 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('Student_model', 'model');
            $data['s_additional_information'] = $this->post('description');
            $student = $this->model->get_by_user($_SESSION['id']);
            $this->model->base_update($student['s_id'], $data);
            echo json_encode(array('result' => 1));
        }
        else {
            echo json_encode(array('result' => 0, 'error' => form_error('description')));
        }
    }

    function update_profile()
    {
        $this->form_validation->set_rules('name_ar', translate('name arabic'), 'required|max_length[25]');
        $this->form_validation->set_rules('name_en', translate('name english'), 'required|max_length[25]');
        $this->form_validation->set_rules('mobile', translate('mobile'), 'required|max_length[25]');
        $this->form_validation->set_rules('email', translate('mobile'), 'required|max_length[25]|callback_email_check');
        if ($this->form_validation->run() == TRUE or FALSE) {
            $this->load->model('Student_model', 'model');
            $this->load->model('User_model');
//            $data['s_additional_information'] = $this->post('description');
            $data['s_facebook'] = $this->post('facebook');
            $data['s_birthdate'] = $this->post('birthday');
            $data['is_complete'] = 1;
            $data_user['u_email'] = $this->post('email');
            $data_user['u_name_ar'] = $this->post('name_ar');
            $data_user['u_name_en'] = $this->post('name_en');
            $data_user['u_mobile'] = $this->post('mobile');
            $data_user['u_telephone'] = $this->post('telephone');
            $data_user['gender'] = $this->post('gender');
            $data_user['country'] = $this->post('country');
            $data_user['city'] = $this->post('city');
            $this->User_model->base_update($_SESSION['id'], $data_user);
            $student = $this->model->get_by_user($_SESSION['id']);
            $this->model->base_update($student['s_id'], $data);
            echo json_encode(array('result' => 1));
        }
        else {
            echo json_encode(array('result' => 0,
                'name_ar' => form_error('name_ar'),
                'name_en' => form_error('name_en'),
                'mobile' => form_error('mobile'),
                'email' => form_error('email'),
                'error' => translate("ERROR")
            ));
        }
    }

    function email_check()
    {
        $this->form_validation->set_message('email_check', translate('This email is already used'));
        $email = $this->post('email');
        $result = $this->db->where(array('u_email' => $email, 'u_id!=' => $_SESSION['id']))->get('users');
        if ($result->num_rows() == 0) return true;
        return false;
    }

    function update_connection()
    {
        $social_medias = $this->db->get('social_media')->result();

        foreach ($social_medias as $social_media) {
            $where = array('social_media' => $social_media->sm_id, 'user' => $_SESSION['id']);

            $array_insert = array('social_media' => $social_media->sm_id, 'user' => $_SESSION['id'], 'usm_value' => $this->post('s_' . $social_media->sm_id));

            $ww = $this->db->where($where)->get('user_social_media');
            if ($ww->num_rows() != 0) {
                $aa = array('usm_value' => $this->post('s_' . $social_media->sm_id));
//                print_r($aa);
                $this->db->where($where)->update('user_social_media', $aa);
            }
            else {
                $this->db->insert('user_social_media', $array_insert);
            }

        }
        echo json_encode(array('result' => 1));

    }

    function change_my_image()
    {
        if (isset($_POST["image"])) {
            $data = $_POST["image"];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $imageName = 'upload/user/user_' . $this->session->userdata('id') . '.png';
            $this->load->model('User_model', 'model');
            $data1['u_profile'] = $imageName;
            $this->model->base_update($this->session->userdata('id'), $data1);
            file_put_contents($imageName, $data);
            chmod($imageName, fileperms($imageName) | 128 + 16 + 2);
//echo 1;
        }
    }
}