<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{
    public $model;

    public function __construct()
    {
        
        parent::__construct();
    }

    public function post($para)
    {
        if (isset($_POST[$para])) {
            return $this->input->post($para);
        }
        return false;


    }

    public function render($page, $title, $data = null)
    {

        $data_header['title'] = translate($title);
        $this->load->view('share/header', $data_header);
        $this->load->view('header/public',$this->header->getNav());
        $this->load->view($page, $data);
        $this->load->view('share/footer');
    }

    public function index()
    {
        
        $this->render('welcome_message', 'Main Page');
    }

    public function register($type = 'student')
    {
        if ($type == "student") {
            $this->render('register/student', 'register student');
        }
        else if ($type == "teacher") {
            $this->render('register/teacher', 'register teacher');

        }
    }

    public function sex_list()
    {
        if (isset($_GET['term'])) {
            $this->db->like('g_title', $_GET['term']);
        }
        $this->db->select('g_id as id , g_title as text');
        $value = $this->db->get('gender')->result();
        echo json_encode($value);
    }

    public function country_list($id = null)
    {
        if (isset($_GET['term'])) {
            $this->db->like('c_name', $_GET['term']);
        }
        $this->db->select('c_id as id , c_name as text');
        $value = $this->db->get('countries')->result();
        if (isset($id)) {
            $value1 = array();
            foreach ($value as $item) {
                $row = array();
                $row['id'] = $item->id;
                $row['text'] = $item->text;
                if ($row['id'] === $id) {
                    $row['selected'] = true;
                }
                $value1[] = $row;
            }
            $value = $value1;

        }


        echo json_encode($value);
    }

    public function city_list($country)
    {
        if (isset($_GET['term'])) {
            $this->db->like('c_name', $_GET['term']);
        }
        $this->db->select('c_id as id , c_name as text');
        $this->db->where('country_id ', $country);
        $value = $this->db->get('city')->result();
        echo json_encode($value);
    }

    public function login()
    {
        $this->render('share/login', translate('login'));
    }

    public function logout()
    {
        session_destroy();
        redirect(base_url());
    }

    public function all_courses()
    {

        $data['countries'] = $this->db->get('countries')->result_array();
        $data['teachers'] = $this->db->join('users', 'teachers.user=users.u_id')->where('users.state', 1)->get('teachers')->result_array();
        $data['training_center'] = $this->db->join('users', 'training_center.user=users.u_id')->where('users.state', 1)->get('training_center')->result_array();
        $data['countries'] = $this->db->get('countries')->result_array();
        $limit = 5;
        $courses_all = $this->db->select('course.* , city.c_name as city_name ,countries.c_name as country_name ')->join('city', 'city.c_id=course.city')->join('countries', 'countries.c_id=city.country_id')->get_where('course', array('state' => 1))->result_array();
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = count($courses_all);
        $config["per_page"] = $limit;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&gt;';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = "&lt;";
        $config["prev_tag_open"] = "<li>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);


        $data['pages_number'] = $this->pagination->create_links();
        $data['count_course'] = count($courses_all);
        $courses = $this->db->order_by("c_id", "ASC")->limit($limit)->select('course.* , city.c_name as city_name ,countries.c_name as country_name ')->join('city', 'city.c_id=course.city')->join('countries', 'countries.c_id=city.country_id')->get_where('course', array('state' => 1))->result_array();
        $result = array();
        foreach ($courses as $cours) {
            $training_centers = $this->db->join('training_center', 'training_center.tc_id=course_training_center.training_center')->join('users', 'users.u_id=training_center.user')->get_where('course_training_center', array('course' => $cours['c_id']))->result_array();
            $teachers = $this->db->join('teachers', 'teachers.t_id=course_teachers.teacher')->join('users', 'users.u_id=teachers.user')->get_where('course_teachers', array('course' => $cours['c_id']))->result_array();
            $maters = $this->db->where_in('m_id', json_decode($cours['c_maters']))->get('mater')->result_array();
            $result [] = array('maters' => $maters, 'course' => $cours, 'training_centers' => $training_centers, 'teachers' => $teachers);

        }
        $data['courses'] = $result;
        $this->render('public/courses/index', 'All Courses', $data);
    }

    public function course($course)
    {

        $courses = $this->db->where('course.c_id', $course)->select('course.* , city.c_name as city_name ,countries.c_name as country_name ')->join('city', 'city.c_id=course.city')->join('countries', 'countries.c_id=city.country_id')->get_where('course', array('state' => 1))->row_array();

        $training_centers = $this->db->join('training_center', 'training_center.tc_id=course_training_center.training_center')->join('users', 'users.u_id=training_center.user')->get_where('course_training_center', array('course' => $course))->result_array();
        $teachers = $this->db->join('teachers', 'teachers.t_id=course_teachers.teacher')->join('users', 'users.u_id=teachers.user')->get_where('course_teachers', array('course' => $course))->result_array();
        $maters = $this->db->where_in('m_id', json_decode($courses['c_maters']))->get('mater')->result_array();
        $result = array('maters' => $maters,
            'course' => $courses,
            'training_centers' => $training_centers,
            'teachers' => $teachers,


        );


        $data['courses'] = $result;
        $data['id'] = $course;
        $this->render('public/courses/course.php', $courses['c_title'], $data);
    }

    function set_comment()
    {
//        print_r($_POST);
        $this->form_validation->set_rules('comment', translate('comment'), 'trim|required|min_length[2]|max_length[1000]');
        $this->form_validation->set_rules('id', translate('id'), 'trim|required');
        if ($this->form_validation->run() == TRUE && (isset($_SESSION['id']) && $_SESSION['user_type'] == 1)) {
            $data['user'] = $_SESSION['id'];
            $data['course'] = $this->post('id');
            $data['cc_comment'] = $this->post('comment');
            $this->db->insert('course_comment', $data);
            echo json_encode(array('result' => 1));
        }
        else {
            if ($this->form_validation->run() == TRUE)
                echo json_encode(array('result' => 0, 'error' => translate('You must be logged  By Account Student.')));
            else
                echo json_encode(array('result' => 0, 'error' => validation_errors()));
        }


    }

    function get_comments()
    {

        $data['insert'] = false;
        $course = $this->input->post('id');
        if (isset($_POST['insert_new'])) {
            $data['insert'] = $_POST['insert_new'];
        }
        $data['comments'] = $this->db->join('users', 'users.u_id=course_comment.user')->where('course', $course)->get('course_comment')->result_array();

        $this->load->view('public/courses/comments', $data);
    }

    // all course
    function ajax_pagination()
    {
        $courses_all = $this->db->select('course.* , city.c_name as city_name ,countries.c_name as country_name ')->join('city', 'city.c_id=course.city')->join('countries', 'countries.c_id=city.country_id')->get_where('course', array('state' => 1))->result_array();
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = count($courses_all);
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&gt;';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = "&lt;";
        $config["prev_tag_open"] = "<li>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        $data['pages_number'] = $this->pagination->create_links();
        $data['count_course'] = count($courses_all);
        $courses = $this->db->order_by("c_id", "ASC")->limit(5, $start)->select('course.* , city.c_name as city_name ,countries.c_name as country_name ')->join('city', 'city.c_id=course.city')->join('countries', 'countries.c_id=city.country_id')->get_where('course', array('state' => 1))->result_array();
        $result = array();
        foreach ($courses as $cours) {
            $training_centers = $this->db->join('training_center', 'training_center.tc_id=course_training_center.training_center')->join('users', 'users.u_id=training_center.user')->get_where('course_training_center', array('course' => $cours['c_id']))->result_array();
            $teachers = $this->db->join('teachers', 'teachers.t_id=course_teachers.teacher')->join('users', 'users.u_id=teachers.user')->get_where('course_teachers', array('course' => $cours['c_id']))->result_array();
            $maters = $this->db->where_in('m_id', json_decode($cours['c_maters']))->get('mater')->result_array();
            $result [] = array('maters' => $maters, 'course' => $cours, 'training_centers' => $training_centers, 'teachers' => $teachers);

        }
        $data['courses'] = $result;
        $this->load->view('public/courses/course_ajax', $data);
    }

    function students()
    {
        $limit = 5;
        $all_students = $this->db->select('countries.c_name as country_name , city.c_name as city_name , student.* , users.*')->join('users', 'users.u_id = student.user')->join('city', 'city.c_id = users.city')->join('countries', 'users.country = countries.c_id')->get('student')->result_array();
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = count($all_students);
        $config["per_page"] = $limit;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&gt;';
        $config["next_tag_open"] = '<li>';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = "&lt;";
        $config["prev_tag_open"] = "<li>";
        $config["prev_tag_close"] = "</li>";
        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);


        $data['pages_number'] = $this->pagination->create_links();
        $data['count_course'] = count($all_students);
        $data['students'] = $all_students;
        $this->render('public/student/index', 'students', $data);
    }

    function student($para)
    {
        $user = $this->db->select('users.* , countries.c_name as country_name')->where('u_id', $para)->join('city','city.c_id=users.city')->join('countries','countries.c_id=users.country')->get('users');
        if ($user->num_rows() == 0) {
            echo "NOT FOUND";
        }
        else {
            $user = $user->row_array();
            if ($user['state'] == 1) {
                // view ...
                $data['user'] = $user;
                $data['student'] = $this->db->where('user', $user['u_id'])->get('student')->row_array();
                $this->render('public/student/show',$user['u_name_ar'],$data);
            }
            else {
                echo 'uesr not active ';
            }

        }
    }

}
