<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Header extends pages
{
    public $model;

    public function __construct()
    {
        
        parent::__construct();
    }

    

    public function index()
    {
        
       $this->load->render('Header/public',$this->header->getNav());

    }
    public function add(){
        $this->form_validation->set_rules('h_title', 'العنوان', 'required');
        $this->form_validation->set_rules('h_url', 'الرابط', 'required');
        $this->form_validation->set_rules('h_date', 'التاريخ', 'required');
        
        if ($this->form_validation->run() == TRUE  ) {
            $set['h_title'] = $this->input->post('h_title');
            $set['h_url'] = $this->input->post('h_url');
            $set['h_date'] = $this->input->post('h_date');
            $set['parent'] = $this->input->post('parent');
            $this->header->create($set);
            $data = array(
                'result' => '1',
                'message_en' => 'success',
                'message_ar' => 'تمت العملية بنجاح',
                
            ); 
            echo json_encode($data);
        }else{
            $data = array(
                'result' => '0',
                'h_title' => form_error('h_title'),
                'h_url' => form_error('h_url'),
                'h_date' => form_error('h_date'),
            ); 
            echo json_encode($data);
        }
       
    }
     public function delete($id){
        var_dump($id);die();
        $this->header->destroy($id);
     }

}
