<?php

require 'Pages.php';

class Users extends Pages
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->model = $this->User_model;
    }

    private function welcome_message($name, $token, $to)
    {
        $message = '  <div style="background: #00A7E5; direction: rtl">
        <br>
        <h1 style="color: white ;width: 100%;text-align: center">اهلا و سهلا ' . $name . '</h1>
        <br>
    </div><div style="text-align: center;color: #00A7E5">
        <h3 >
            يرجي تأكيد عملية تسجيل عن طريق الضغط على زر التأكيد
        </h3>
        <br>
        <br>
        <a href="' . base_url('users/active/' . $token) . '" style=" text - decoration: none;padding: 20px;padding - right: 40px;padding - left: 40px;border:1px #00A7E5 solid ; color: #00A7E5">
            تأكيد
            </a >
    </div >';

        send_email(translate('welcome'), $message, $to);


    }

    function register($para = null, $para2 = null)
    {
        if (!isset($para)) {
//            parent::register();
        }
        else if ($para == 'student') {
//echo  translate('email');
            $this->form_validation->set_rules('email', translate('email'), 'required|min_length[5]|max_length[25]|is_unique[users.u_email]');
            $this->form_validation->set_rules('password', translate('password'), 'required|min_length[8]|max_length[25]');
            $this->form_validation->set_rules('arabicname', translate('arabicname'), 'required|min_length[2]|max_length[25]');
            $this->form_validation->set_rules('englishname', translate('englishname'), 'required|min_length[2]|max_length[25]');
            $this->form_validation->set_rules('mobile', translate('mobile'), 'required|min_length[10]|max_length[25]');
            $this->form_validation->set_rules('telephone', translate('telephone'), 'max_length[15]');
            $this->form_validation->set_rules('sex', translate('gender'), 'required|max_length[99]');
            $this->form_validation->set_rules('country', translate('country'), 'required|max_length[99]');
            $this->form_validation->set_rules('city', translate('city'), 'required|max_length[99]');


            if ($this->form_validation->run() === TRUE) {
                $data['u_email'] = $this->post('email');
                $token = md5(time() . $data['u_email']);
                $data['u_password'] = md5($this->post('password'));
                $data['u_name_en'] = $this->post('arabicname');
                $data['u_name_ar'] = $this->post('englishname');
                $data['u_mobile'] = $this->post('mobile');
                $data['u_telephone'] = $this->post('telephone');
                $data['gender'] = $this->post('sex');
                $data['country'] = $this->post('country');
                $data['city'] = $this->post('city');
                $data['user_type'] = 1;
                $data['state'] = 2;
                $data['token'] = $token;
                $student['user'] = $this->model->base_add($data);
                $this->load->model('Student_model');
                $insert = $this->Student_model->base_add($student);
                if ($insert > 0) {
                    echo json_encode(array('result' => 1));
                    $this->welcome_message($data['u_name_ar'], $token, $data['u_email']);
                    return;
                }
                echo json_encode(array('result' => -1));
                return;

            }
            else {

                $data['result'] = 0;
                $data['email'] = form_error('email');
                $data['password'] = form_error('password');
                $data['arabicname'] = form_error('arabicname');
                $data['englishname'] = form_error('englishname');
                $data['mobile'] = form_error('mobile');
                $data['telephone'] = form_error('telephone');
                $data['sex'] = form_error('sex');
                $data['country'] = form_error('country');
                $data['city'] = form_error('city');
                $data['facebook'] = form_error('facebook');
                echo json_encode($data);
            }

        }
        else if ($para == "teacher") {
            $this->form_validation->set_rules('email', translate('email'), 'required|min_length[5]|max_length[25]|is_unique[users.u_email]');
            $this->form_validation->set_rules('password', translate('password'), 'required|min_length[8]|max_length[25]');
            $this->form_validation->set_rules('arabicname', translate('arabicname'), 'required|min_length[2]|max_length[25]');
            $this->form_validation->set_rules('englishname', translate('englishname'), 'required|min_length[2]|max_length[25]');
            $this->form_validation->set_rules('mobile', translate('mobile'), 'required|min_length[10]|max_length[25]');
            $this->form_validation->set_rules('telephone', translate('telephone'), 'max_length[15]');
            $this->form_validation->set_rules('sex', translate('gender'), 'required|max_length[99]');
            $this->form_validation->set_rules('country', translate('country'), 'required|max_length[99]');
            $this->form_validation->set_rules('city', translate('city'), 'required|max_length[99]');


            if ($this->form_validation->run() === TRUE) {
                $data['u_email'] = $this->post('email');
                $token = md5(time() . $data['u_email']);
                $data['u_password'] = md5($this->post('password'));
                $data['u_name_en'] = $this->post('arabicname');
                $data['u_name_ar'] = $this->post('englishname');
                $data['u_mobile'] = $this->post('mobile');
                $data['u_telephone'] = $this->post('telephone');
                $data['gender'] = $this->post('sex');
                $data['country'] = $this->post('country');
                $data['city'] = $this->post('city');
                $data['user_type'] = 2;
                $data['state'] = 2;
                $data['token'] = $token;
                $teacher['user'] = $this->model->base_add($data);
                $this->load->model('Teacher_model');
                $insert = $this->Teacher_model->base_add($teacher);
                if ($insert > 0) {
                    echo json_encode(array('result' => 1));
                    $this->welcome_message($data['u_name_ar'], $token, $data['u_email']);
                    return;
                }
                echo json_encode(array('result' => -1));
                return;

            }
            else {

                $data['result'] = 0;
                $data['email'] = form_error('email');
                $data['password'] = form_error('password');
                $data['arabicname'] = form_error('arabicname');
                $data['englishname'] = form_error('englishname');
                $data['mobile'] = form_error('mobile');
                $data['telephone'] = form_error('telephone');
                $data['sex'] = form_error('sex');
                $data['country'] = form_error('country');
                $data['city'] = form_error('city');
                $data['facebook'] = form_error('facebook');
                echo json_encode($data);
            }
        }

    }

    function chack_user()
    {
        $this->form_validation->set_rules('email', translate('email'), 'required|min_length[5]|max_length[25]');
        $this->form_validation->set_rules('password', translate('password'), 'required|min_length[8]|max_length[25]');
        if ($this->form_validation->run() == TRUE) {
            $data['u_email'] = $this->post('email');
            $data['u_password'] = md5($this->post('password'));
            $user = $this->db->get_where('users', $data);
//            echo $this->db->last_query();
            if ($user->num_rows() > 0) {

                $user = $user->row_array();
                if ($user['state'] != 1) {
                    echo json_encode(array('result' => -2));
                    return;
                }
                else {
                    $_SESSION['name'] = array();
                    $_SESSION['name']['arabic'] = $user['u_name_ar'];
                    $_SESSION['name']['english'] = $user['u_name_en'];

                    $_SESSION['id'] = $user['u_id'];
                    $_SESSION['user_type'] = $user['user_type'];
                    $_SESSION['is_login'] = true;
                    if ($user['user_type'] == 1) {

                        $this->load->model('Student_model');
                        $student = $this->Student_model->get_by_user($user['u_id']);
                        $_SESSION['is_complete'] = $student['is_complete'];

                    }
                    else if ($user['user_type'] == 2) {

                        $this->load->model('Teacher_model');
                        $teacher = $this->Teacher_model->get_by_user($user['u_id']);
                        $_SESSION['is_complete'] = $teacher['is_complete'];

                    }
                    else if ($user['user_type'] == 4) {

                    }

                    echo json_encode(array('result' => 1));
                    return;
                }
            }

            echo json_encode(array('result' => -1));
            return;
        }
        else {
            echo json_encode(array('result' => -1));
            return;
        }

    }

    function active($token)
    {
        $this->db->where('token', $token);
        $this->db->update('users', array('state' => 1));
        redirect(base_url('?verify=3'));
    }

    function update_password()
    {
        if (!isset($_SESSION['id'])) {
            echo json_encode(array('result' => 0, 'error' => 'NOT  LOGIN'));
        }
        else {
            $this->form_validation->set_message('password_check', translate('incorrect password'));
            $this->form_validation->set_rules('old_password', translate('Old password'), 'required|min_length[8]|max_length[50]|callback_password_check');
            $this->form_validation->set_rules('new_password', translate('New password'), 'required|min_length[8]|max_length[50]');
            $this->form_validation->set_rules('confirm_password', translate('Confirm password'), 'required|min_length[8]|max_length[50]|matches[new_password]');
            if ($this->form_validation->run() == TRUE) {
                $data['u_id'] = $_SESSION['id'];
                $data['u_password'] = md5($this->post('old_password'));
                $new['u_password'] = md5($this->post('confirm_password'));
                $this->db->where($data)->update('users', $new);
                echo json_encode(array('result' => 1,
                    'old_password' => "",
                    'new_password' => "",
                    'confirm_password' => "",
                ));
            }
            else {
                echo json_encode(array('result' => 0, 'error' => "ERROR",
                    'old_password' => form_error('old_password'),
                    'new_password' => form_error('new_password'),
                    'confirm_password' => form_error('confirm_password'),
                ));
            }
        }
    }

    function password_check()
    {
        $this->form_validation->set_message('password_check', translate('incorrect password'));
        $data['u_id'] = $_SESSION['id'];
        $data['u_password'] = md5($this->post('old_password'));

        $result = $this->db->get_where('users', $data);
//        echo $this->db->last_query();
        if ($result->num_rows() == 0) return false;
        return true;
    }
}